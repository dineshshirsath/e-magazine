<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class HomeModel extends CI_Model
{
	public function getSearchListData()
	{
		$query = $this->db->select('m.*, c.category_name')
				->from('tbl_magazine as m')
				->join('tbl_category as c', 'c.id = m.category_id', 'left')
				->order_by('m.id','desc')
				->limit(5)
				->get()->result_array();
		return $query;
	}
	public function getSearchData($searchData)
	{
		$query = $this->db->select('m.*, c.category_name')
				->from('tbl_category as c')
				->join('tbl_magazine as m', 'm.category_id = c.id', 'left')
				->where('c.category_name',$searchData)
				->get()->result_array();
		return $query;
	}
	public function queryAllData($lastID)
	{
		$query = $this->db->select('COUNT(*) as num_rows')
				->from('tbl_magazine')
				->where('id <',$lastID)
				->order_by('id','desc')
				->get()->result_array();
		return $query;
	}
	public function queryData($lastID)
	{
		$query = $this->db->select('m.*, c.category_name')
				->from('tbl_magazine as m')
				->join('tbl_category as c', 'c.id = m.category_id', 'left')
				->where('m.id <',$lastID)
				->order_by('m.id','desc')
				->limit(2)
				->get()->result_array();
		return $query;
	}
}
?>
	  	
