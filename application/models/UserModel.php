<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class UserModel extends CI_Model
{
	public function getUserListingData()
	{
		$query = $this->db->select('*')
				->from('tbl_users')
				->get()->result_array();

		return $query;	
	}

	public function getProfileData()
	{
		$query = $this->db->select('*')
				->from('tbl_users')
				->where('id', $this->session->userdata('user_id'))
				->get()->result_array();

		return $query;
	}

	public function getAuthorDataUsinSessionId()
	{
		$query = $this->db->select('*')
				->from('tbl_users')
				->where('id', $this->session->userdata('user_id'))
				->get()->result_array();
		
		return $query;	
	}

	public function update($id, $data)
	{
		$this->db->where('id', $id);
		if ($this->db->update('tbl_users', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function changePassword($id, $data)
	{
		$this->db->where('id', $id);
		if ($this->db->update('tbl_users', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function changeProfileImageUpdate($id, $data)
	{
		$this->db->where('id', $id);
		if ($this->db->update('tbl_users', $data)) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function checkProfileImage($id)
	{
		$query = $this->db->select('*')
			->from('tbl_users')
			->where('id', $id)
			->get()->result_array();

		return $query;
	}
}
?>
