<?php defined('BASEPATH') OR exit('No direct script access allowed');
class MagazineModel extends CI_Model{
      
	function __construct(){
		parent::__construct();
	}	
	public function pagedata($id = null)
	{
		$this->db->select('m.*,mp.*,c.*');
		$this->db->from('tbl_magazine as m');
		$this->db->join('tbl_magazine_pages as mp','m.id=mp.magazine_id');
		$this->db->join('tbl_category as c','c.id=m.category_id');
		$this->db->where('m.id',$id);
		$this->db->order_by('order', 'asc');
		return $this->db->get()->result_array();	
	}
	public function magazineList()
	{
		$this->db->select('m.*,c.id as catId, c.category_name ');
		$this->db->from('tbl_magazine as m');
		$this->db->join('tbl_category as c','c.id=m.category_id');
		return $this->db->get()->result_array();
		//return $this->db->get('tbl_magazine')->result_array();	
	}
	public function getMagazinePageCount($mid)
	{
		$this->db->where('magazine_id',$mid);
	    $this->db->get('tbl_magazine_pages')->result_array();
	    return $this->db->affected_rows();
	}

	public function getMagazineDadaById($magazineId)
	{
		$query = $this->db->select('*')
		        ->from('tbl_magazine')
		        ->where('id', $magazineId)
		        ->get()->result_array();

		return $query;
	}

	public function insertDuplicateMagazine($data)
	{
		if ($this->db->insert('tbl_magazine', $data)){
			return true;
		}else{
			return flase;
		}
	}

	public function getMagazinePageContentById($magazineId)
	{
		$query = $this->db->select('*')
		        ->from('tbl_magazine_pages')
		        ->where('magazine_id', $magazineId)
		        ->get()->result_array();

		return $query;
	}

	public function insertDuplicateMagazinePageContent($data)
	{
		if ($this->db->insert('tbl_magazine_pages', $data)){
			return true;
		}else{
			return flase;
		}
	}
	
	public function delete($id)
	{
		$this->db->where('id', $id);
		if ($this->db->delete('tbl_magazine')) {
			$this->db->where('magazine_id', $id);
			if ($this->db->delete('tbl_magazine_pages')) {
				return true;
			}else{
				return flase;
			}
		}else{
			return flase;
		}
	}

	public function insertNewPageContent($data)
	{
		if ($this->db->insert('tbl_new_page', $data)){
			return true;
		}else{
			return flase;
		}
	}
	public function newPageData($id)
	{
	  	$query = $this->db->select('*')
	  			->from('tbl_new_page')
	  			->where('id', $id)
	  			->get()->result_array();
	  	return $query;
	}
	public function getMagazineCount($catId)
	{
		$query = $this->db->select('COUNT(category_id) as total')
	  			->from('tbl_magazine')
	  			->where('category_id', $catId)
	  			->get()->result_array();
	  	return $query;
	}
}
?>