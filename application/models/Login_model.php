<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Login_model extends CI_Model{
      
	function __construct(){
		parent::__construct();
	}	
	public function checkLogin()
	{
		$this->db->where(['email'=>$this->input->post('username'),'password'=>$this->input->post('password')]);
		$result = $this->db->get('tbl_users');
		if($result->num_rows() > 0){
			$user = $result->row_array();			
			$this->session->set_userdata([
				'user_name'=>$user['first_name'],
				'user_email'=>$user['email'],
				'user_id'=>$user['id']
			]);
			return 1;
		}else{
			return 0;
		}		
	}
}
?>