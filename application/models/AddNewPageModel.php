<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class AddNewPageModel extends CI_Model
{
	public function insert($data)
	{
		if ($this->db->insert_batch('tbl_magazine', $data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function insertMagazineContent($data)
	{
		if ($this->db->insert('tbl_magazine_pages', $data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getDataAlreadyExists($magazineId, $pageNumber)
	{
		$query = $this->db->select('*')
			->from('tbl_magazine_pages')
			->where('magazine_id', $magazineId)
			->where('page_number', $pageNumber)
			->get();

		return $query->row();
	}

	public function getMagazineDataById($magazineId)
	{
		$query = $this->db->select('m.*, mp.*')
			->from('tbl_magazine as m')
			->where('m.id', $magazineId)
			->join('tbl_magazine_pages as mp','mp.magazine_id = m.id', 'left')
			->get()->result_array();

		return $query;
	}

	public function updateMagazinContent($data, $id)
	{
		$this->db->where('id', $id);
		if ($this->db->update('tbl_magazine_pages', $data))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function getCategoryData()
	{
		$query = $this->db->select('id, category_name')
				->from('tbl_category')
				->get();

		return $query->result_array();	
	}
	public function getMagazinePageData($magazineId, $pageNumber)
	{
		$query = $this->db->select('*')
			->from('tbl_magazine_pages')
			->where('magazine_id', $magazineId)
			->where('page_number', $pageNumber)
			->get()->result_array();

		return $query;
	}
	public function GetExpiryDate()
	{
		$query = $this->db->select('*')
			->from('tbl_magazine')
			->get()->result_array();
		return $query;
	}
	public function update($Id,$data)
	{
		$this->db->where('id',$Id);
		if($this->db->update('tbl_magazine',$data)) {
			return true;
			
		}
		else{
			return false;
		}
	}

	public function deleteMagazineContent($pageNumber, $magazineId)
	{
		$this->db->where('magazine_id',$magazineId);
		$this->db->where('page_number ',$pageNumber);
		if($this->db->delete('tbl_magazine_pages')) {
			$query = $this->db->select('m.number_of_page, mp.*')
				->where('m.id', $magazineId)
				->join('tbl_magazine_pages as mp', 'mp.magazine_id = m.id', 'left')
				->get('tbl_magazine as m')->result_array();
			return $query;
		}
		else{
			return false;
		}
	}
	
	public function updateNOP($magazineId, $updateData)
	{
		$this->db->where('id',$magazineId);
		if($this->db->update('tbl_magazine',$updateData)) {
			return true;
		}
		else{
			return false;
		}
	}

	public function updatePageNumber($magazineId, $rowId, $data)
	{
		$this->db->where('id',$rowId);
		$this->db->where('magazine_id',$magazineId);
		if($this->db->update('tbl_magazine_pages',$data)) {
			return true;
		}
		else{
			return false;
		}
	}

	public function setPageOrder($magazineId, $pageNumber, $data)
	{
		$this->db->where('magazine_id',$magazineId);
		$this->db->where('page_number',$pageNumber);
		if($this->db->update('tbl_magazine_pages',$data)) {
			return true;
		}
		else{
			return false;
		}
	}
}