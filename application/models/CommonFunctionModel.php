<?php defined('BASEPATH') OR exit('No direct script access allowed');
class CommonFunctionModel extends CI_Model{
      
	function __construct(){
		parent::__construct();
	}	
	
	function checkUser()
	{
		$this->db->where('id',$this->session->userdata('user_id'));
		return $this->db->get('tbl_users')->row();
	}
	function checkProfileImage($id)
	{
		$query = $this->db->select('*')
			->from('tbl_users')
			->where('id', $id)
			->get()->result_array();

		return $query;
	}
}
?>