<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


//Route Of Admin Side
$route['admin'] = ADMIN.'login/index';
$route['admin-login'] = ADMIN.'login/index';
$route['admin-logout'] = ADMIN.'login/logout';
$route['admin-dashboard'] = ADMIN.'home/index';
$route['add-new-page'] = ADMIN.'AddNewPageController/index';
$route['add_blank'] = ADMIN.'AddNewPageController/add_blank';
$route['load_templates'] = ADMIN.'AddNewPageController/load_templates';
$route['update_block_info'] = ADMIN.'AddNewPageController/update_block_info';
$route['template_import'] = ADMIN.'AddNewPageController/template_import';
$route['add-page'] = ADMIN.'AddNewPageController/add_page';
$route['add-page-store'] = ADMIN.'AddNewPageController/add_page_insert';
$route['magazine-listing'] = ADMIN.'MagazineController/index';
$route['delete-magazine'] = ADMIN.'AddNewPageController/deleteMagazine';
$route['duplicate-magazine'] = ADMIN.'MagazineController/duplicateMagazine';
$route['delete-magazine-content/(:any)'] = ADMIN.'MagazineController/delete/$1';
$route['new-page'] = ADMIN.'MagazineController/newPage';

//New page web page builder
$route['create-new-page'] = ADMIN.'WebPageBuilderController/index';


//user Profile
$route['listing-user'] = ADMIN.'UserController/index';
$route['user-profile'] = ADMIN.'UserController/userProfile';
$route['user-profile-update'] = ADMIN.'UserController/update';
$route['user-change-password'] = ADMIN.'UserController/changePassword';
$route['profile-image'] = ADMIN.'UserController/changeProfileImage';


// Category
$route['category-listing'] = ADMIN.'CategoryController/index';
$route['add-category'] = ADMIN.'CategoryController/add';
$route['store-category'] = ADMIN.'CategoryController/insert';
$route['edit-category/(:any)'] = ADMIN.'CategoryController/updateView/$1';
$route['delete-category/(:any)'] = ADMIN.'CategoryController/delete/$1';

$route['magazine'] = 'welcome/index';

$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
