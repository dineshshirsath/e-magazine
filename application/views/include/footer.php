<!-- /.content-wrapper -->
		  	<footer class="main-footer">
			    <div class="pull-right hidden-xs">
			      	
			    </div>
		    	<strong>Copyright &copy; <?php echo date("Y"); ?> All Rights Reserved by <a href="http://orbs-solutions.com/" target="_blank">Orbs Solutions</a>.
		  	</footer>
			<!-- Control Sidebar -->
		  	<aside class="control-sidebar control-sidebar-dark">
		    	<!-- Create the tabs -->
		    	<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
		      		<li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
		      		<li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
		    	</ul>
		    	<!-- Tab panes -->
		    	<div class="tab-content">
		      		<!-- Home tab content -->
			      	<div class="tab-pane" id="control-sidebar-home-tab"></div>
			      	<!-- /.tab-pane -->
		    	</div>
		  	</aside>
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
		</div>
		<!-- jQuery 3 -->
		
		<!-- Bootstrap 3.3.7 -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/fastclick.js"></script>
		<script src="<?php echo base_url() ?>assets/js/adminlte.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/jquery.sparkline.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/dashboard2.js"></script>
		<script src="<?php echo base_url() ?>assets/js/demo.js"></script>
		
		
		


	</body>
</html>
