            <div class="headernav" style="margin-bottom: 20px;">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-1 col-xs-3 col-sm-2 col-md-2 logo "><a href="index.html"><img src="assets/website/images/logo.jpg" alt=""  /></a></div>
                        <!-- <div class="col-lg-3 col-xs-9 col-sm-5 col-md-3 selecttopic">
                            <div class="dropdown">
                                <a data-toggle="dropdown" href="#" >Borderlands 2</a> <b class="caret"></b>
                                <ul class="dropdown-menu" role="menu">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Borderlands 1</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-2" href="#">Borderlands 2</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-3" href="#">Borderlands 3</a></li>

                                </ul>
                            </div>
                        </div> -->
                        <div class="col-lg-4 search hidden-xs hidden-sm col-md-3">
                            <div class="wrap">
                                <form action="javascript:void" method="post" class="form">
                                    <div class="pull-left txt">
                                        <input type="text" id="search" class="form-control" placeholder="Search" required="">
                                    </div>
                                    <div class="pull-right">
                                        <button class="btn btn-default" onclick="myFunction()"><i class="fa fa-search"></i></button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                        <!-- <div class="col-lg-4 col-xs-12 col-sm-5 col-md-4 avt">
                            <div class="stnt pull-left">                            
                                <form action="03_new_topic.html" method="post" class="form">
                                    <button class="btn btn-primary">Start New Topic</button>
                                </form>
                            </div>
                            <div class="env pull-left"><i class="fa fa-envelope"></i></div>

                            <div class="avatar pull-left dropdown">
                                <a data-toggle="dropdown" href="#"><img src="assets/website/images/avatar.jpg" alt="" /></a> <b class="caret"></b>
                                <div class="status green">&nbsp;</div>
                                <ul class="dropdown-menu" role="menu">
                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#">My Profile</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-2" href="#">Inbox</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-3" href="#">Log Out</a></li>
                                    <li role="presentation"><a role="menuitem" tabindex="-4" href="04_new_account.html">Create account</a></li>
                                </ul>
                            </div>
                            
                            <div class="clearfix"></div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8" id="getData">
                        <!-- POST -->
                    <?php $i = 1; foreach ($searchListData as $row): ?>
                        <?php $postID = $row["id"]; 
                            $CreatedPageCnt = $this->MagazineModel->getMagazinePageCount($row['id']); 
                            $date = date("Y-m-d");
                            $expiryDate = $row['expiry_date'];
                        ?>
                        <?php if ($CreatedPageCnt >= $row['number_of_page']): ?>
                            <?php if ($date <= $expiryDate): ?>
                                <div class="post">
                                    <div class="wrap-ut pull-left">
                                        <div class="userinfo pull-left">
                                            <div class="avatar">
                                                <img src="assets/website/images/maga.jpg" alt="" style="height: 35px; width: 35px;">
                                                <div class="status green">&nbsp;</div>
                                            </div>
                                            <!-- <div class="icons">
                                                <h4><?php echo $i++ ?></h4>
                                            </div> -->
                                        </div>
                                        <div class="posttext pull-left">
                                            <h2><a href="<?php echo base_url().'magazine?id='.base64_encode($row['id']);?>" target="_blank""><?php echo $row['magazine_name'] ?></a></h2>
                                            <p><b>Category :</b> <?php echo $row['category_name'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Expiry :</b> <?php echo $row['expiry_date'] ?></p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="postinfo pull-left">
                                        <div class="comments">
                                            <div class="commentbg">
                                                <p><?php echo $row['number_of_page'] ?> Pages</p>
                                                <div class="mark"></div>
                                            </div>
                                        </div>
                                        <!-- <div class="views"><i class="fa fa-eye"></i> 1,568</div>
                                        <div class="time"><i class="fa fa-clock-o"></i> 24 min</div> -->
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                            <?php endif ?>                            
                        <?php endif ?>
                    <?php endforeach ?> 
                    <div class="load-more" lastID="<?php echo $postID; ?>" style="display: none;">
                    </div>                    
                        
                    </div>
                    <div class="col-lg-4 col-md-4">

                        <!-- -->
                        <div class="sidebarblock">
                            <h3>Categories</h3>
                            <div class="divline"></div>
                            <div class="blocktxt">
                                <ul class="cats">
                                    <?php foreach ($categoryData as $val): ?>
                                        <?php $totalMagazine = $this->MagazineModel->getMagazineCount($val['id']); ?>
                                        <li><a href="#"><?php echo $val['category_name'] ?> <span class="badge pull-right"><?php echo $totalMagazine[0]['total'] ?></span></a></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <!-- <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-xs-12">
                            <div class="pull-left"><a href="#" class="prevnext"><i class="fa fa-angle-left"></i></a></div>
                            <div class="pull-left">
                                <ul class="paginationforum">
                                    <li class="hidden-xs"><a href="#">1</a></li>
                                    <li class="hidden-xs"><a href="#">2</a></li>
                                    <li class="hidden-xs"><a href="#">3</a></li>
                                    <li class="hidden-xs"><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">6</a></li>
                                    <li><a href="#" class="active">7</a></li>
                                    <li><a href="#">8</a></li>
                                    <li class="hidden-xs"><a href="#">9</a></li>
                                    <li class="hidden-xs"><a href="#">10</a></li>
                                    <li class="hidden-xs hidden-md"><a href="#">11</a></li>
                                    <li class="hidden-xs hidden-md"><a href="#">12</a></li>
                                    <li><a href="#">1586</a></li>
                                </ul>
                            </div>
                            <div class="pull-left"><a href="#" class="prevnext last"><i class="fa fa-angle-right"></i></a></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div> -->
            </section>

<script>
function myFunction() 
{
    var searchData = $('#search').val();
    var url = "<?php echo base_url() ?>Welcome/getSearchData";
    if (searchData) {
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                'searchData' : searchData 
            },
            success: function (response) {
                $('#getData').html(response);
            }
        });
    }else{
        var error = "Plase enter the category..!";
        $('#search').attr('placeholder', error);
    }
}
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $(window).scroll(function(){
        var lastID = $('.load-more').attr('lastID');
        var url = "<?php echo base_url() ?>Welcome/getScrollLoadData";
        if(($(window).scrollTop() == $(document).height() - $(window).height()) && (lastID != 0)){
            $.ajax({
                type:'POST',
                url: url,
                data:'id='+lastID,
                beforeSend:function(){
                    $('.load-more').show();
                },
                success:function(response){
                    $('.load-more').remove();
                $('#getData').append(response);
                    //$('#postList').append(html);
                }
            });
        }
    });
});
</script>
