<div class="content-wrapper" style="min-height: 1126.3px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	    <h1>
                Admin Dashboard
	    </h1>
    	<ol class="breadcrumb">
	        <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?php echo base_url(); ?>category-listing">Category Listing</a></li>
	        <li class="active">Update Category</li>
      	</ol>
    </section>
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				 <div class="row">
					<div class="col-md-12">
				        <!-- general form elements -->
				        <div class="box box-primary">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Update Category</h3>
				            </div>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <form action="<?php echo base_url(); ?>admin/CategoryController/update" method="post" enctype="multipart/form-data" role="form">
				              	<div class="box-body">
					                <div class="col-md-12">
					                	<div class="form-group">
					                	<input type="hidden" class="form-control" name="id" id="id" value="<?php echo $categoryUpdatData[0]['id'] ?>">
					                	</div>
					                </div>				                	
						            <div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">Category Name</label>
											<input type="text" class="form-control" name="category_name" id="category_name" value="<?php echo $categoryUpdatData[0]['category_name'] ?>" placeholder="Title">
					                	</div>
					                	<span style="color: red;"><?php echo form_error('category_name');?></span>
						            </div>	
						            <div class="col-md-12">	    
						                <div class="form-group">
							                <label for="exampleInputPassword1">Description</label>
								            <textarea id="editor01" class="form-control" name="category_description"><?php echo $categoryUpdatData[0]['category_description'] ?>
                                        	</textarea>
                                        	<span style="color: red;"><?php echo form_error('category_description');?></span>
						            	</div>
						            </div>
						            </div>
				              	</div>
				              	<!-- /.box-body -->
				              	<div class="box-footer">
				                	<button type="submit" class="btn btn-primary">Update</button>
				              	</div>
				            </form>
				        </div>
				    <!-- /.box -->
				    </div>
			    </div>
			</div>
		</div>
	</section>
</div>
