<div class="content-wrapper" style="min-height: 1126.3px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	    <h1> 
	    		Author Dashboard
	    </h1>
    	<ol class="breadcrumb">
	        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li><a href="<?php echo base_url(); ?>category-listing">Category Listing</a></li>
	        <li class="active">Add Category</li>
      	</ol>
    </section>
    <section class="content">
    	<div class="container-fluid">
			<div class="row">
				 <div class="row">
				 <?php if($this->session->flashdata('success')){ ?>
					<div class="alert alert-success">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
					   <?php echo $this->session->flashdata('success'); ?>
					</div>

				<?php } else if($this->session->flashdata('error')){  ?>

					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
					   <?php echo $this->session->flashdata('error'); ?>
					</div>

					<?php } ?>
					<div class="col-md-12">
				        <!-- general form elements -->
				        <div class="box box-primary">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Add Category</h3>
				            </div>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <form action="<?php echo base_url(); ?>store-category" method="post" enctype="multipart/form-data" role="form">
				              	<div class="box-body">
					                <div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">Category Name</label>
											<input type="text" class="form-control" name="category_name" id="category_name" value="<?php echo set_value("category_name"); ?>" placeholder="Category Name">
											<span style="color: red;"><?php echo form_error('category_name');?></span>
					                	</div>
						            </div>	
						            <div class="col-md-12">	    
						                <div class="form-group">
							                <label for="exampleInputPassword1">Description</label>
								            <textarea id="editor01" class="form-control textarea" name="category_description"><?php echo set_value('category_description');?>
                                        	</textarea>
                                        	<span style="color: red;"><?php echo form_error('category_description');?></span>
						                </div>
						            </div>
						                
				              	<!-- /.box-body -->
				              	<div class="box-footer">
				                	<button type="submit" class="btn btn-primary">Submit</button>
				              	</div>
				            </form>
				        </div>
				    <!-- /.box -->
				    </div>
			    </div>
			</div>
		</div>
	</section>
</div>
