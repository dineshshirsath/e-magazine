<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
                Admin Dashboard
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Category Listing</li>
          </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Info Content -->
	        <?php if($this->session->flashdata('success')){ ?>
	        <div class="alert alert-success">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	           <?php echo $this->session->flashdata('success'); ?>
	        </div>

	        <?php } else if($this->session->flashdata('error')){  ?>
	        <div class="alert alert-danger">
	            <a href="#" class="close" data-dismiss="alert">&times;</a>
	            <?php echo $this->session->flashdata('error'); ?>
	        </div>
	        <?php } ?>
         <div class="box">
            <div class="box-header">
                  <h3 class="box-title">Category Listing</h3>
                  <a style="float: right;" href="<?php echo base_url(); ?>add-category" class="btn btn-primary">
                    <span>
                        Add Category
                    </span>
                    </a> 
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped dataTable" id="category">
                    <thead>
                        <th>Sr. No.</th>
                        <th>Category Name</th>
                        <th>Description</th>
                        <th>Action</th>
                    </thead>
                    <?php $inc = 1;
                        foreach($categoryListingData as $row)
                        {?>
                        <tr>
                            <td><?php echo $inc++;?></td>
                            <td><?php echo $row['category_name'];?></td>
                            <td><?php echo $row['category_description'];?></td>
                            <td>
                                <div class="action-btns">
                                    <a href="<?php echo base_url(); ?>edit-category/<?php echo $row['id']; ?>" title="Edit" class="btn-lg edit-btn"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <a href="<?php echo base_url(); ?>delete-category/<?php echo $row['id']; ?>" title="Delete" class="btn-lg delete-btn" data-toggle="modal"><i class="fa fa-trash-o fa-lg" aria-hidden="true" onClick="return doconfirm();"></i></a>
                                </div>
                            </td>
                        </tr>
                        <?php }
                        ?>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
    <!-- /.content -->
</div>
<script>
      $(function(){
          $('#category').dataTable({
            "processing": true,
            "bDestroy": true,
            "aaSorting": [[ 0,"asc" ]],
            "columnDefs":[
                {
                    "targets":[0,3],
                    "orderable":false,
                },
            ],
          });
      });
</script>
<script>
function doconfirm()
{
    job=confirm("Are you sure to delete record permanently?");
    if(job!=true)
    {
        return false;
    }
}
</script>