<html>
<head>
  <meta charset="utf-8">
	    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/page/AdminLTE.min.css">
	    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/page/_all-skins.min.css">
	    <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/page/demo.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/css/page/email-editor.bundle.min.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/css/page/colorpicker.css" rel="stylesheet" />

        <link href="<?php echo base_url(); ?>assets/css/page/editor-color.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/page/sweetalert2.min.css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style>

img#img_preview {
    height: 191px;
    width: 150px;
}
li.setting-item.preview {
    margin-left: 40px;
}
li.setting-item.save-template {
    margin-left: 40px;
}
li.setting-item.other-devices {
    margin-left: 40px;
}
</style>
<body class="skin-blue" data-spy="scroll" data-target="#scrollspy">
<div class="wrapper">
  <header class="main-header">

    		<!-- Logo -->
		    <a href="javascript:void()" class="logo">
		      	<!-- mini logo for sidebar mini 50x50 pixels -->
		      	<span class="logo-mini"><b>E</b>M</span>
		      		<!-- logo for regular state and mobile devices -->
		      	<span class="logo-lg"><b>E</b>Magazine</span>
		    </a>

    		<!-- Header Navbar: style can be found in header.less -->
    		<nav class="navbar navbar-static-top">
      		<!-- Sidebar toggle button-->
			      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			        	<span class="sr-only">Toggle navigation</span>
			      </a>
			      <!-- Navbar Right Menu -->
			      <div class="navbar-custom-menu">
			      	 	<ul class="nav navbar-nav">
			      	 	<?php
  $imageData = $this->CommonFunctionModel->checkProfileImage($this->session->userdata('user_id')); 
?>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <?php if (!empty($imageData[0]['image'])){ ?>
              <img src="<?php echo base_url() ?>assets/image/profile_Image/<?php echo $imageData[0]['image']; ?>" class="user-image" alt="User Image">
               <?php }else{ ?>                  
                <img src="<?php echo base_url() ?>assets/image/profile_Image/profilepicture.jpg" class="user-image" alt="User Image">
              <?php } ?>
              <span class="hidden-xs"><?php echo $this->session->userdata('user_name'); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <?php if (!empty($imageData[0]['image'])){ ?>
                  <img src="<?php echo base_url() ?>assets/image/profile_Image/<?php echo $imageData[0]['image']; ?>" class="img-circle" alt="User Image">
                <?php }else{ ?>                   
                  <img src="<?php echo base_url() ?>assets/image/profile_Image/profilepicture.jpg" class="img-circle" alt="User Image">
                <?php } ?>
                <p>
                  <?php echo $this->session->userdata('user_name'); ?>
                  <small></small>
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url(); ?>user-profile" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url() ?>admin-logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
        <div class="elements-db" style="display:none">
            <div class="tab-elements element-tab active">
                <ul class="elements-accordion">
                    <li class="elements-accordion-item" data-type="typography"><a class="elements-accordion-item-title">Typography</a><div class="elements-accordion-item-content"><ul class="elements-list"><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-header"></i></div><div class="elements-item-name">The Page Header</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="1" data-types="background,padding"  data-last-type="background"><table align="center" border="0" cellpadding="0" cellspacing="0" class="main" style="width:100%">
	<tbody>
		<tr>
			<td style="background-color:#ffffff" class="element-content" contenteditable="true">
			<h1>Page Header</h1>

			<h4>Your subtitle</h4>
			</td>
		</tr>
	</tbody>
</table>
</div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-header"></i></div><div class="elements-item-name">Heading</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="2" data-types="background,padding"  data-last-type="background"><table class="main" width="100%" cellspacing="0" cellpadding="0" border="0"  align="center" >
    <tbody>
        <tr>
            <td align="left" class="element-content" style="padding-left:50px;padding-right:50px;padding-top:10px;padding-bottom:10px;background-color:#FFFFFF;">
                <h1 contenteditable="true" style="font-weight: normal;">Header</h1>
            </td>
        </tr>
    </tbody>
</table>
</div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-paragraph"></i></div><div class="elements-item-name">paragraph</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="3" data-types="background,border-radius,padding"  data-last-type="background"><table  class="main" width="100%" cellspacing="0" cellpadding="0" border="0"
                 
                   style="background-color:#FFFFFF" align="center">
                <tbody>
                    <tr>
                        <td class="element-content" align="left"
                            style="padding-left:50px;padding-right:50px;padding-top:10px;padding-bottom:10px;font-family:Arial;font-size:13px;color:#000000;line-height:22px">
                            <div contenteditable="true" class="test-text">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table></div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-align-left"></i></div><div class="elements-item-name">Image left text</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="4" data-types="background,padding,image-settings"  data-last-type="background"><table  class="main" cellspacing="0" cellpadding="0" border="0"
                  
                   style="background-color:#FFFFFF" align="center">
                <tbody>
                    <tr>
                        <td class="element-content" contenteditable="true"
                            style="padding:10 50 10 50px;font-family:Arial;font-size:13px;color:#000000;line-height:22px;text-align:left">

                            <img class="content-image" align="left"
                            src="https://placeholdit.imgix.net/~text?txtsize=33&amp;txt=350%C3%97150&amp;w=350&amp;h=150"
                                 style="display: inline-block;margin: 5px;  padding: 0 0 0 0;Width:350px;height:150px;border-width: 0px;border-color: #000000;border-style: solid;">

                            <div style="margin: 0px 0px 10px 0px; line-height: 22px;" >
                                Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table></div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-columns"></i></div><div class="elements-item-name">2 column text</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="5" data-types="background,border-radius,padding,image-settings"  data-last-type="background"><table class="main" cellspacing="0" cellpadding="0" border="0"   style="background-color:#FFFFFF" align="center">
    <tbody>
        <tr>
            <td class="element-content" align="left" style="padding: 10px 50px; font-family: Arial; font-size: 13px; color: rgb(0, 0, 0); line-height: 22px;">
                <table align="left" width="49%">
                    <tbody>
                        <tr>
                            <td contenteditable="true" align="center" class="element-contenteditable active">

                                <img border="0" class="content-image " src="https://placeholdit.imgix.net/~text?txtsize=33&amp;txt=250%C3%97150&amp;w=240&amp;h=150" style="display: inline-block;margin: 0px;width:240px;height:150px;border-width: 0px;border-color: #000000;border-style: solid;">
                                <div style="margin: 0px 0px 10px 0px; line-height: 22px;padding: 10px 0 0 0;text-align:left">
                                    Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis
                                    aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <table align="right" width="50%">
                    <tbody>
                        <tr>
                            <td contenteditable="true" align="center">
                                <img border="0" class="content-image " src="https://placeholdit.imgix.net/~text?txtsize=33&amp;txt=250%C3%97150&amp;w=240&amp;h=150" style="display: inline-block;margin: 0px;width:240px;height:150px;border-width: 0px;border-color: #000000;border-style: solid;">

                                <div style="margin: 0px 0px 10px 0px; line-height: 22px;padding: 10px 0 0 0;text-align:left">
                                    Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis
                                    aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>

                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td></td>
        </tr>
    </tbody>
</table>
</div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-align-right"></i></div><div class="elements-item-name">Image right text</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="6" data-types="background,border-radius,padding,image-settings"  data-last-type="background"><table  class="main" cellspacing="0" cellpadding="0" border="0"
               
                   style="background-color:#FFFFFF" align="center">
                <tbody>
                    <tr>
                        <td class="element-content"   contenteditable="true"
                            style="text-align:left;padding-left:50px;padding-right:50px;padding-top:10px;padding-bottom:10px;font-family:Arial;font-size:13px;color:#000000;line-height:22px">

                            <img border="0" align="right" class="content-image "
                                 src="https://placeholdit.imgix.net/~text?txtsize=33&amp;txt=350%C3%97150&amp;w=350&amp;h=150"
                                 style="display: block;margin: 5px;padding:0 0 0 0px;width:350px;height:150px;border-width: 0px;border-color: #000000;border-style: solid;">

                            <div style="margin: 0px 0px 10px 0px; line-height: 22px;" >
                               Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </div>

                        </td>
                    </tr>
                </tbody>
            </table></div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-minus"></i></div><div class="elements-item-name">unordered list</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="7" data-types="background,border-radius,padding"  data-last-type="background"><table align="center" border="0" cellpadding="0" cellspacing="0" class="main" style="background-color:#ffffff; width:100%">
	<tbody>
		<tr>
			<td class="element-content" contenteditable="true">
			<div>
			<ul style="margin-left:0px; margin-right:0px">
				<li style="list-style-type:disc">Item 1</li>
				<li style="list-style-type:disc">Item 2</li>
			</ul>
			</div>
			</td>
		</tr>
	</tbody>
</table>
</div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-list-ol"></i></div><div class="elements-item-name">ordered list</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="8" data-types="background,border-radius,padding"  data-last-type="background"><table  class="main" width="100%" cellspacing="0" cellpadding="0" border="0"
                  
      
                   style="background-color:#FFFFFF" align="center">
                <tbody>
                    <tr>
                        <td class="element-content" align="left"
                            style="padding-left:50px;padding-right:50px;padding-top:10px;padding-bottom:10px;font-family:Arial;font-size:13px;color:#000000;line-height:22px">
                            <div contenteditable="true" style="padding:0 0 0 20px ">
                                <ol style="padding: 0;margin: 0;  display: inline-block;">
                                    <li style="list-style-type: decimal;">Item 1</li>
                                    <li style="list-style-type: decimal;">Item 2</li>
                                </ol>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table></div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-header"></i></div><div class="elements-item-name">jumbotron</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="9" data-types="background,padding"  data-last-type="background"><table class="main" width="100%" cellspacing="0" cellpadding="0" border="0" 
align="center" >
    <tbody>
        <tr>
            <td align="left" class="element-content" style="padding-left:50px;padding-right:50px;padding-top:10px;padding-bottom:10px;background-color:#FFFFFF;">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td>
                            <h1 contenteditable="true" style="font-weight: normal;text-align:center">Welcome to Builder</h1>
                        </td>
                    </tr>
                    <tr>
                        <td contenteditable="true" align="center">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                        </td>
                    </tr>
                    <tr>
                        <td contenteditable="true" align="center" style="padding-left:50px;padding-right:50px;padding-top:10px;padding-bottom:10px;font-family:Arial;font-size:13px;color:#000000;line-height:22px;">
                            <a contenteditable="true" style="margin-top: 10px;background-color: #3498DB;font-family: Arial;color: #FFFFFF;display: inline-block;border-radius: 6px;text-align: center; padding: 12px 20px;text-decoration:none" class="button-1 hyperlink" href="#" data-default="1">
                                  Click me
                              </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
</div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-certificate"></i></div><div class="elements-item-name">features</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="10" data-types="background,padding,hyperlink"  data-last-type="background"><table class="main" width="100%" cellspacing="0" cellpadding="0" border="0" 
 align="center" >
    <tbody>
        <tr>
            <td align="left" class="element-content" style="padding:10px;background-color:#FFFFFF;text-align:center">

                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td align="left" style="padding: 0 10px;">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td>
                                        <h1 contenteditable="true" style="font-weight: normal;text-align:center">Header</h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td contenteditable="true" align="center" style="font-family: Arial;font-size: 13px;color: #000000;line-height: 22px;">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                                    </td>
                                </tr>
                                <tr>
                                    <td contenteditable="true" align="center" style="padding:10px 0;font-family:Arial;font-size:13px;color:#000000;line-height:22px;">
                                        <a contenteditable="true" style="margin-top: 10px;background-color: #3498DB;font-family: Arial;color: #FFFFFF;display: inline-block;border-radius: 6px;text-align: center; padding: 12px 20px;text-decoration:none" class="button-1 hyperlink" href="#" data-default="1">
                                          Click me
                                      </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" style="padding: 0 10px;">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td>
                                        <h1 contenteditable="true" style="font-weight: normal;text-align:center">Header</h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td contenteditable="true" align="center" style="font-family: Arial;font-size: 13px;color: #000000;line-height: 22px;">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                                    </td>
                                </tr>
                                <tr>
                                    <td contenteditable="true" align="center" style="padding:10px 0;font-family:Arial;font-size:13px;color:#000000;line-height:22px;">
                                        <a contenteditable="true" style="margin-top: 10px;background-color: #3498DB;font-family: Arial;color: #FFFFFF;display: inline-block;border-radius: 6px;text-align: center; padding: 12px 20px;text-decoration:none" class="button-1 hyperlink" href="#" data-default="1">
                                          Click me
                                      </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td align="left" style="padding: 0 10px;">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                <tr>
                                    <td>
                                        <h1 contenteditable="true" style="font-weight: normal;text-align:center">Header</h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td contenteditable="true" align="center" style="font-family: Arial;font-size: 13px;color: #000000;line-height: 22px;">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                                    </td>
                                </tr>
                                <tr>
                                    <td contenteditable="true" align="center" style="padding:10px 0;font-family:Arial;font-size:13px;color:#000000;line-height:22px;">
                                        <a contenteditable="true" style="margin-top: 10px;background-color: #3498DB;font-family: Arial;color: #FFFFFF;display: inline-block;border-radius: 6px;text-align: center; padding: 12px 20px;text-decoration:none" class="button-1 hyperlink" href="#" data-default="1">
                                          Click me
                                      </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>



            </td>
        </tr>
    </tbody>
</table>
</div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-life-ring"></i></div><div class="elements-item-name">Service List</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="11" data-types="background,padding,image-settings"  data-last-type="background"><table class="main" width="100%" cellspacing="0" cellpadding="0" border="0" 
align="center" >
    <tbody>
        <tr>
            <td align="left" class="element-content" style="padding:5px;background-color:#FFFFFF;">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td contenteditable="true" style="text-align:center">

                            <img border="0" class="content-image" src="http://emailbuilder.cidcode.net/demo-v2/assets/images/service-list/lamp.png" style="display: inline-block;margin: 0px;width:64px;height:64px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h4 contenteditable="true" style="font-weight: normal;text-align:center">Generating ideas</h4>
                        </td>
                    </tr>
                    <tr>
                        <td contenteditable="true" align="center" style="font-family: Arial;font-size: 13px;color: #000000;line-height: 22px;">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left" class="element-content" style="padding:5px;background-color:#FFFFFF;">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td contenteditable="true" style="text-align:center">

                            <img border="0" class="content-image" src="http://emailbuilder.cidcode.net/demo-v2/assets/images/service-list/comp.png" style="display: inline-block;margin: 0px;width:64px;height:64px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h4 contenteditable="true" style="font-weight: normal;text-align:center">Web Design</h4>
                        </td>
                    </tr>
                    <tr>
                        <td contenteditable="true" align="center" style="font-family: Arial;font-size: 13px;color: #000000;line-height: 22px;">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left" class="element-content" style="padding:5px;background-color:#FFFFFF;">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td contenteditable="true" style="text-align:center">

                            <img border="0" class="content-image" src="http://emailbuilder.cidcode.net/demo-v2/assets/images/service-list/code.png" style="display: inline-block;margin: 0px;width:64px;height:64px">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h4 contenteditable="true" style="font-weight: normal;text-align:center">Web Development</h4>
                        </td>
                    </tr>
                    <tr>
                        <td contenteditable="true" align="center" style="font-family: Arial;font-size: 13px;color: #000000;line-height: 22px;">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
</div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-usd"></i></div><div class="elements-item-name">Price table</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="12" data-types="background,padding"  data-last-type="background"><table class="main" width="100%" cellspacing="0" cellpadding="0" border="0" 

align="center" 
>
    <tbody>
        <tr>
            <td align="left" class="element-content" style="padding:10px;background-color:#FFFFFF;">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td contenteditable="true" style="text-align:center;background-color:#DFF0D8;font-weight:bold; color:#3C763D;   padding: 10px 0;">
                            Basic
                        </td>
                    </tr>
                    <tr>
                        <td contenteditable="true" style="text-align:center;background-color:#3C763D;padding: 10px 0;color:#fff">
                            <span style="font-size:24px"><b>$20</b></span> monthly
                        </td>
                    </tr>
                    <tr>
                        <td contenteditable="true" align="center">
                            <div contenteditable="true" style="">

                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">10 Gb Space Storage</div>
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">10 Gb Bandwith</div>
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">1 E-mail Addresses</div>
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">1 Domain Names</div>
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">Free Updates</div>
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">24/7 Support</div>

                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td contenteditable="true" align="center" style="padding:10px 0 0 0;font-family:Arial;font-size:13px;color:#000000;line-height:22px;">
                            <a contenteditable="true" style="margin-top: 10px;background-color: #5CB85C;font-family: Arial;color: #FFFFFF;display: inline-block;border-radius: 6px;text-align: center; padding: 12px 20px;text-decoration:none" class="button-1 hyperlink" href="#" data-default="1">
                                  SIGN UP
                              </a>
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left" class="element-content" style="padding:10px;background-color:#FFFFFF;">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td contenteditable="true" style="text-align:center;background-color:#F2DEDE;font-weight:bold;color:#AF4442;padding: 10px 0;">
                            PROMO
                        </td>
                    </tr>
                    <tr>
                        <td contenteditable="true" style="text-align:center;background-color:#AF4442;padding: 10px 0;color:#fff">
                            <span style="font-size:24px"><b>$29</b></span> monthly
                        </td>
                    </tr>
                    <tr>
                        <td contenteditable="true" align="center">
                            <div contenteditable="true" style="">
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">10 Gb Space Storage</div>
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">10 Gb Bandwith</div>
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">1 E-mail Addresses</div>
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">1 Domain Names</div>
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">Free Updates</div>
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">24/7 Support</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td contenteditable="true" align="center" style="padding:10px 0 0 0;font-family:Arial;font-size:13px;color:#000000;line-height:22px;">
                            <a contenteditable="true" style="margin-top: 10px;background-color: #D9534F;font-family: Arial;color: #FFFFFF;display: inline-block;border-radius: 6px;text-align: center; padding: 12px 20px;text-decoration:none" class="button-1 hyperlink" href="#" data-default="1">
                                  SIGN UP
                              </a>
                        </td>
                    </tr>
                </table>
            </td>

            <td align="left" class="element-content" style="padding:10px;background-color:#FFFFFF;">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td contenteditable="true" style="text-align:center;background-color:#D9EDF7;font-weight:bold;color:#31708F;padding: 10px 0;">
                            ADVANCED
                        </td>
                    </tr>
                    <tr>
                        <td contenteditable="true" style="text-align:center;background-color:#31708F;padding: 10px 0;color:#fff">
                            <span style="font-size:24px"><b>$29</b></span> monthly
                        </td>
                    </tr>
                    <tr>
                        <td contenteditable="true" align="center">
                            <div contenteditable="true" style="">
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">10 Gb Space Storage</div>
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">10 Gb Bandwith</div>
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">1 E-mail Addresses</div>
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">1 Domain Names</div>
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">Free Updates</div>
                                <div style="list-style-type: none;border-bottom:1px dotted #ddd;padding: 5px;">24/7 Support</div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td contenteditable="true" align="center" style="padding:10px 0 0 0;font-family:Arial;font-size:13px;color:#000000;line-height:22px;">
                            <a contenteditable="true" style="margin-top: 10px;background-color: #5BC0DE;font-family: Arial;color: #FFFFFF;display: inline-block;border-radius: 6px;text-align: center; padding: 12px 20px;text-decoration:none" class="button-1 hyperlink" href="#" data-default="1">
                                  SIGN UP
                              </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
</div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-asterisk"></i></div><div class="elements-item-name">underlined</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="30" data-types="background,padding,border-radius,image-settings,hyperlink"  data-last-type="background"><p><u>underlined</u></p>
</div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-bell"></i></div><div class="elements-item-name">Rating</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="33" data-types="background,padding,border-radius,image-settings,hyperlink"  data-last-type="background"><table class="main" style="width:100%" cellspacing="1" cellpadding="1" border="1">
	<tbody>
		<tr>
			<td class="element-content" contenteditable="true"><a class="button-1 hyperlink" href="http://google.com" style="background-color:#d9534f;border-radius:6px;color:#ffffff;display:inline-block;font-family:Arial;margin-top:10px;padding:12px 20px;text-align:center;text-decoration:none;">My Link</a></td>
		</tr>
	</tbody>
</table>
</div></div></div> </div></div></li></ul></div></li><li class="elements-accordion-item" data-type="media"><a class="elements-accordion-item-title">Media</a><div class="elements-accordion-item-content"><ul class="elements-list"><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-picture-o"></i></div><div class="elements-item-name">Image</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="13" data-types="background,padding,border-radius,image-settings"  data-last-type="background"><table  class="main" width="100%" cellspacing="0" cellpadding="0" border="0"
                   align="center"
                 >
                <tbody>
                    <tr>
                        <td align="center" class="image image-full element-content" contenteditable="true"
                            style="padding:0;">
                            <img  class="content-image " style="width: 100%;height:200px;border-width: 0px;border-color: #000000;border-style: solid;"
                            src="https://placeholdit.imgix.net/~text?txtsize=50&txt=image&w=600&h=200" />
                        </td>
                    </tr>
                </tbody>
            </table></div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-columns"></i></div><div class="elements-item-name">2 Image</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="14" data-types="background,border-radius,padding,image-settings"  data-last-type="background"><table  class="main" cellspacing="0" cellpadding="0" border="0"               
                   style="background-color:#FFFFFF" align="center">
                <tbody>
                    <tr>
                        <td class="element-content"   align="center">
                          <table>
                            <tr>
                                  <td contenteditable="true" align="center"
                                  style="padding:10px 25px">
                              <img border="0" class="content-image" src="https://placeholdit.imgix.net/~text?txtsize=33&amp;txt=250%C3%97150&amp;w=240&amp;h=150" style="display: inline-block;margin: 0px;width:100%;height:150px;border-width: 0px;border-color: #000000;border-style: solid;">

                                </td>
                                <td contenteditable="true" align="center"    style="padding:10px 25px">

                                <img border="0" class="content-image" src="https://placeholdit.imgix.net/~text?txtsize=33&amp;txt=250%C3%97150&amp;w=240&amp;h=150" style="display: inline-block;margin: 0px;width:100%;height:150px;border-width: 0px;border-color: #000000;border-style: solid;">

                                </td>
                            </tr>
                          </table>
                    </tr>
                </tbody>
            </table>
</div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-video-camera"></i></div><div class="elements-item-name">Video</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="15" data-types="background,padding,youtube-frame"  data-last-type="background"><table  class="main" width="100%" cellspacing="0" cellpadding="0" border="0"
              
                   style="background-color:#FFFFFF" align="center">
                <tbody>
                    <tr>
                        <td class="element-content youtube-frame" align="left"
                            style="padding-left:50px;padding-right:50px;padding-top:10px;padding-bottom:10px;font-family:Arial;font-size:13px;color:#000000;line-height:22px">
                            		<a href="#" 
   style="text-decoration:none;display:none" 
   class="nonplayable-ytb" 
   target="_blank">
   <img src="<?php echo base_url(); ?>assets/images/mqdefault.jpg" height="274" width="498" />
</a>

<embed id="youtube-iframe" width="100%" height="315" base="https://www.youtube.com/v/" wmode="opaque" id="swfContainer0" type="application/x-shockwave-flash" src="https://www.youtube.com/v/6oAqOFz6QsU?border=0&client=ytapi-google-gmail&version=3&start=0">
</embed>

		
                        </td>
                    </tr>
                </tbody>
            </table></div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-bolt"></i></div><div class="elements-item-name">dfs</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="29" data-types="background,padding,border-radius,image-settings,hyperlink"  data-last-type="background"><p><strong>dfSFGGGGGGGGGGGGGGGG</strong></p>
</div></div></div> </div></div></li></ul></div></li><li class="elements-accordion-item" data-type="layout"><a class="elements-accordion-item-title">Layout</a><div class="elements-accordion-item-content"><ul class="elements-list"><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-minus"></i></div><div class="elements-item-name">Divider</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="16" data-types=""  data-last-type=""><table class="main"   width="100%"  cellspacing="0" cellpadding="0" border="0" align="center" style="background-color:#FFFFFF">
                <tbody>
                    <tr>
                        <td class="divider-simple" style="padding-left:50px;padding-right:50px;padding-top:10px;padding-bottom:10px;background-color:#FFFFFF;">
                            <div style="border-top: 1px solid #DADFE1;"></div>
                        </td>
                    </tr>
                </tbody>
            </table></div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-minus"></i></div><div class="elements-item-name">Divider (dotted)</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="17" data-types=""  data-last-type=""><table class="main" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="background-color:#FFFFFF">
    <tbody>
        <tr>
            <td class="divider-simple" style="padding-left:50px;padding-right:50px;padding-top:10px;padding-bottom:10px;background-color:#FFFFFF;">
                <div style="border-top: 1px dotted #DADFE1;"></div>
            </td>
        </tr>
    </tbody>
</table>
</div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-minus"></i></div><div class="elements-item-name">Divider (dashed)</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="18" data-types=""  data-last-type=""><table class="main" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" style="background-color:#FFFFFF">
    <tbody>
        <tr>
            <td class="divider-simple" style="padding-left:50px;padding-right:50px;padding-top:10px;padding-bottom:10px;background-color:#FFFFFF;">
                <div style="border-top: 1px dashed #DADFE1;"></div>
            </td>
        </tr>
    </tbody>
</table>
</div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-globe"></i></div><div class="elements-item-name">View in browser</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="19" data-types="background,padding"  data-last-type="background"><table class="main" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" >
    <tbody>
        <tr>
            <td align="left" class="page-header element-content" style="padding-left:50px;padding-right:50px;padding-top:10px;padding-bottom:10px;background-color:#FFFFFF;text-align:center">

                <a href="#view_web" contenteditable="true" class="view-web" style="margin:5px 0">View in browser </a>
            </td>
        </tr>
    </tbody>
</table></div></div></div> </div></div></li></ul></div></li><li class="elements-accordion-item" data-type="button"><a class="elements-accordion-item-title">Button</a><div class="elements-accordion-item-content"><ul class="elements-list"><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-square-o"></i></div><div class="elements-item-name">Button</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="20" data-types="background,border-radius,padding,button"  data-last-type="background"><table  class="main" width="100%" cellspacing="0" cellpadding="0" border="0"
                   
                   style="background-color:#FFFFFF" align="center">
                <tbody>
                    <tr>
                        <td class="element-content button-1" align="left" style="padding-left:50px;padding-right:50px;padding-top:10px;padding-bottom:10px;font-family:Arial;font-size:13px;color:#000000;line-height:22px;">

                            <a   style="  margin-top: 10px;background-color: #3498DB;font-family: Arial;color: #FFFFFF;display: inline-block;border-radius: 6px;text-align: center; padding: 12px 20px;text-decoration:none" class="button-1 hyperlink" href="#" data-default="1">
                                Click me
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table></div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-square-o"></i></div><div class="elements-item-name">2 Button</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="21" data-types="background,border-radius,padding"  data-last-type="background"><table  class="main" width="100%" cellspacing="0" cellpadding="0" border="0"
                 
                   style="background-color:#FFFFFF" align="center">
                <tbody>
                    <tr>
                        <td class="element-content" align="left"
                            style="padding-left:50px;padding-right:50px;padding-top:10px;padding-bottom:10px;font-family:Arial;font-size:13px;color:#000000;line-height:22px;">

                            <a contenteditable="true" style="margin-top: 10px;background-color: #3498DB;font-family: Arial;color: #FFFFFF;display: inline-block;border-radius: 6px;text-align: center; padding: 12px 20px;text-decoration:none" class="button-1" href="#" data-default="1">
                                Button 1
                            </a>

                            <a contenteditable="true" style="margin-top: 10px;background-color: #3498DB;font-family: Arial;color: #FFFFFF;display: inline-block;border-radius: 6px;text-align: center; padding: 12px 20px;text-decoration:none" class="button-1" href="#" data-default="1">
                                Button 2
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
</div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-square-o"></i></div><div class="elements-item-name">3 Button</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="22" data-types="background,border-radius,padding"  data-last-type="background"><table  class="main" width="100%" cellspacing="0" cellpadding="0" border="0"
     
                   style="background-color:#FFFFFF" align="center">
                <tbody>
                    <tr>
                        <td class="element-content" align="left"
                            style="padding-left:50px;padding-right:50px;padding-top:10px;padding-bottom:10px;font-family:Arial;font-size:13px;color:#000000;line-height:22px;">

                            <a contenteditable="true" style="  margin-top: 10px; background-color: #3498DB;font-family: Arial;color: #FFFFFF;display: inline-block;border-radius: 6px;text-align: center; padding: 12px 20px;text-decoration:none" class="button-1" href="#" data-default="1">
                                Button 1
                            </a>

                            <a contenteditable="true" style="  margin-top: 10px;background-color: #3498DB;font-family: Arial;color: #FFFFFF;display: inline-block;border-radius: 6px;text-align: center; padding: 12px 20px;text-decoration:none" class="button-1" href="#" data-default="1">
                                Button 2
                            </a>
                            <a contenteditable="true" style="  margin-top: 10px;background-color: #3498DB;font-family: Arial;color: #FFFFFF;display: inline-block;border-radius: 6px;text-align: center; padding: 12px 20px;text-decoration:none" class="button-1" href="#" data-default="1">
                                Button 3
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table></div></div></div> </div></div></li></ul></div></li><li class="elements-accordion-item" data-type="social"><a class="elements-accordion-item-title">Social</a><div class="elements-accordion-item-content"><ul class="elements-list"><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-share-square-o"></i></div><div class="elements-item-name">Social links 1</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="23" data-types="social-content,background,padding"  data-last-type="social-content"><table class="main" width="100%" cellspacing="0" cellpadding="0" border="0"  align="center" style="background-color:#FFFFFF;">
    <tbody>
        <tr>
            <td class="element-content social-content" style="padding:20px 50px;text-align:center">
                <a href="#instagram1" style="border: none;display: inline-block;margin-top: 10px;" class="instagram">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/insta-01.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="pinterest">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/pin-01.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="google-plus">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/gplus-01.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="facebook">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/fb-01.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="twitter">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/twt-01.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="linkedin">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/in-01.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="youtube">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/ytb-01.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="skype">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/skype-01.png" width="32">
                </a>
            </td>
        </tr>
    </tbody>
</table>
</div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-share"></i></div><div class="elements-item-name">Social links 2</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="24" data-types="social-content,background,padding"  data-last-type="social-content"><table class="main" width="100%" cellspacing="0" cellpadding="0" border="0"  align="center" style="background-color:#FFFFFF;">
    <tbody>
        <tr>
            <td class="element-content social-content" style="padding:20px 50px;text-align:center">
                <a href="#insta2" style="border: none;display: inline-block;margin-top: 10px;" class="instagram">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/insta-02.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="pinterest">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/pin-02.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="google-plus">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/gplus-02.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="facebook">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/fb-02.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="twitter">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/twt-02.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="linkedin">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/in-02.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="youtube">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/ytb-02.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="skype">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/skype-02.png" width="32">
                </a>
            </td>
        </tr>
    </tbody>
</table>
</div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-share-square"></i></div><div class="elements-item-name">Social links 3</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="25" data-types="social-content,background,padding"  data-last-type="social-content"><table class="main" width="100%" cellspacing="0" cellpadding="0" border="0" align="center"  style="background-color:#FFFFFF;">
    <tbody>
        <tr>
            <td class="element-content  social-content" style="padding:20px 50px;text-align:center">
                <a href="#insta3" style="border: none;display: inline-block;margin-top: 10px;" class="instagram">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/insta-03.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="pinterest">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/pin-03.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="google-plus">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/gplus-03.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="facebook">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/fb-03.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="twitter">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/twt-03.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="linkedin">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/in-03.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="youtube">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/ytb-03.png" width="32">
                </a>
                <a href="#" style="border: none;display: inline-block;margin-top: 10px;" class="skype">
                    <img border="0" src="<?php echo base_url(); ?>assets/images/skype-03.png" width="32">
                </a>
            </td>
        </tr>
    </tbody>
</table>
</div></div></div> </div></div></li></ul></div></li><li class="elements-accordion-item" data-type="footer"><a class="elements-accordion-item-title">Footer</a><div class="elements-accordion-item-content"><ul class="elements-list"><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-minus"></i></div><div class="elements-item-name">Adress</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="26" data-types="background,padding"  data-last-type="background"><table class="main" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" >
    <tbody>
        <tr>
            <td align="left" class="page-header element-content" style="padding-left:50px;padding-right:50px;padding-top:10px;padding-bottom:10px;background-color:#FFFFFF;text-align:center">
                <div contenteditable="true">
                    Orbs Solutions PVT LTD
                </div>
            </td>
        </tr>
    </tbody>
</table>
</div></div></div> </div></div></li><li><div class="elements-list-item"><div class="preview"><div class="elements-item-icon"> <i class="fa fa-minus"></i></div><div class="elements-item-name">Adress with Logo</div></div><div class="view"><div class="sortable-row"><div class="sortable-row-container"> <div class="sortable-row-actions"><div class="row-move row-action"><i class="fa fa-arrows-alt"></i></div><div class="row-remove row-action"><i class="fa fa-remove"></i></div><div class="row-duplicate row-action"><i class="fa fa-files-o"></i></div><div class="row-code row-action"><i class="fa fa-code"></i></div></div><div class="sortable-row-content"  data-id="27" data-types="background,padding,image-settings"  data-last-type="background"><table class="main" width="100%" cellspacing="0" cellpadding="0" border="0" align="center" >
    <tbody>
        <tr>
            <td align="left" class="page-header element-content" style="padding-left:50px;padding-right:50px;padding-top:10px;padding-bottom:10px;background-color:#FFFFFF;text-align:center">

                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td style="text-align:left" contenteditable="true">
                            <img border="0" class="content-image" src="<?php echo base_url(); ?>assets/images/text.png" style="display: inline-block;margin:0px;width:150px;height:50px">
                        </td>
                        <td contenteditable="true" style="text-align:right">
                            Orbs Solutions PVT LTD
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </tbody>
</table>
</div></div></div> </div></div></li></ul></div></li>                </ul>
            </div>
        </div>
        <div class="editor">

        </div>

        <!--<div id="previewModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Preview</h4>
                    </div>
                    <div class="modal-body">
                        <div class="">
                            <label for="">URL : </label> <span class="preview_url"></span>
                        </div>
                        <iframe id="previewModalFrame" width="100%" height="400px"></iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>-->
        <div id="demp"></div> 
        
</div><!-- ./wrapper -->
  <menu type="context" id="popup-menu">
     <menuitem type="command" disabled>Custom menu</menuitem>
    <menuitem type="command" label="Create New Page" onclick="newPage()"></menuitem>
  </menu>
</body>
</html>
<script src="<?php echo base_url(); ?>assets/js/page/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/page/jquery-ui.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/page/jquery.nicescroll.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

        <!--for ace editor  -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/ace/1.1.01/ace.js" type="text/javascript"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/ace/1.1.01/theme-monokai.js" type="text/javascript"></script>

        <!--for tinymce  -->
        <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=7hpaoprwtrtsznk8g6n41lcydeoiyssppgyt7o48jv5wmd0m"></script>
        <script src="<?php echo base_url(); ?>assets/js/page/sweetalert2.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/page/colorpicker.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/page/email-editor-plugin.js"></script>


        <!--for bootstrap-tour  -->
        <script src="<?php echo base_url(); ?>assets/js/page/bootstrap-tour.min.js"></script>
        
        <script>
            var _is_demo = true;

            function loadImages() {
                $.ajax({
                    url: 'get-files.php',
                    type: 'GET',
                    dataType: 'json',
                    success: function(data) {
                        if (data.code == 0) {
                            _output = '';
                            for (var k in data.files) {
                                if (typeof data.files[k] !== 'function') {
                                    _output += "<div class='col-sm-3'>" +
                                        "<img class='upload-image-item' src='" + data.directory + data.files[k] + "' alt='" + data.files[k] + "' data-url='" + data.directory + data.files[k] + "'>" +
                                        "</div>";
                                    // console.log("Key is " + k + ", value is" + data.files[k]);
                                }
                            }
                            $('.upload-images').html(_output);
                        }
                    },
                    error: function() {}
                });
            }

            var _templateListItems;

            var _emailBuilder = $('.editor').emailBuilder({
                //new features begin

                showMobileView: true,
                onTemplateDeleteButtonClick: function(e, dataId, parent) {

                    $.ajax({
                        url: 'delete_template.php',
                        type: 'POST',
                        data: {
                            templateId: dataId
                        },
                        //	dataType: 'json',
                        success: function(data) {
                            parent.remove();
                        },
                        error: function() {}
                    });
                },
                //new features end

                lang: 'en',
                elementsHTML: $('.elements-db').html(),
                langJsonUrl: '<?php echo base_url() ?>application/views/admin/add-new-page/lang-1.json',
                loading_color1: 'red',
                loading_color2: 'green',
                showLoading: false,

                blankPageHtmlUrl: '<?php echo base_url() ?>application/views/admin/add-new-page/template-blank-page.html',
                loadPageHtmlUrl: '<?php echo base_url() ?>application/views/admin/add-new-page/template-load-page.html',

                //left menu
                showElementsTab: true,
                showPropertyTab: true,
                showCollapseMenu: true,
                showBlankPageButton: true,
                showCollapseMenuinBottom: true,

                //setting items
                showSettingsBar: true,
                showSettingsPreview: true,
                showSettingsExport: true,
                showSettingsImport: true,
                showSettingsSendMail: true,
                showSettingsSave: true,
                showSettingsLoadTemplate: true,

                //show context menu
                showContextMenu: true,
                showContextMenu_FontFamily: true,
                showContextMenu_FontSize: true,
                showContextMenu_Bold: true,
                showContextMenu_Italic: true,
                showContextMenu_Underline: true,
                showContextMenu_Strikethrough: true,
                showContextMenu_Hyperlink: true,

                //show or hide elements actions
                showRowMoveButton: true,
                showRowRemoveButton: true,
                showRowDuplicateButton: true,
                showRowCodeEditorButton: true,
                onSettingsImportClick: function() {

                    $('#popupimport').modal('show');

                },
                onBeforePopupBtnImportClick: function() {
                    console.log('onBeforePopupBtnImportClick html');
                    var file_data = $('.input-import-file').prop('files')[0];
                    var form_data = new FormData();
                    form_data.append('importfile', file_data);

                    $.ajax({
                        url: '<?php echo base_url();?>template_import',
                        dataType: 'json',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function(response) {

                            _data = response;
                            //  _data = JSON.parse(response);
                            $('.content-wrapper .email-editor-elements-sortable').html('');

                            $('#demp').html(_data.content);

                            _content = '';
                            $('#demp .main').each(function(index, item) {
                                _content += '<div class="sortable-row">' +
                                    '<div class="sortable-row-container">' +
                                    ' <div class="sortable-row-actions">';

                                _content += '<div class="row-move row-action">' +
                                    '<i class="fa fa-arrows-alt"></i>' +
                                    '</div>';


                                _content += '<div class="row-remove row-action">' +
                                    '<i class="fa fa-remove"></i>' +
                                    '</div>';


                                _content += '<div class="row-duplicate row-action">' +
                                    '<i class="fa fa-files-o"></i>' +
                                    '</div>';


                                _content += '<div class="row-code row-action">' +
                                    '<i class="fa fa-code"></i>' +
                                    '</div>';

                                _content += '</div>' +

                                    '<div class="sortable-row-content" >' +

                                    '</div></div></div>';

                                $('.content-wrapper .email-editor-elements-sortable').append(_content);
                                $('.content-wrapper .email-editor-elements-sortable .sortable-row').eq(index).find('.sortable-row-content').append(item);
                            });
                        }
                    });
                },
                onElementDragStart: function(e) {},
                onElementDragFinished: function(e, contentHtml, dataId) {
                    $.ajax({
                        url: '<?php echo base_url();?>update_block_info',
                        type: 'POST',
                        data: {
                            block_id: dataId
                        },
                        dataType: 'json',
                        success: function(data) {

                        },
                        error: function() {}
                    });

                },

                onBeforeRowRemoveButtonClick: function(e) {
                    console.log('onBeforeRemoveButtonClick html');

                    /*
                      if you want do not work code in plugin ,
                      you must use e.preventDefault();
                    */
                    //e.preventDefault();
                },
                onAfterRowRemoveButtonClick: function(e) {
                    console.log('onAfterRemoveButtonClick html');
                },
                onBeforeRowDuplicateButtonClick: function(e) {
                    console.log('onBeforeRowDuplicateButtonClick html');
                    //e.preventDefault();
                },
                onAfterRowDuplicateButtonClick: function(e) {
                    console.log('onAfterRowDuplicateButtonClick html');
                },
                onBeforeRowEditorButtonClick: function(e) {
                    console.log('onBeforeRowEditorButtonClick html');
                    //e.preventDefault();
                },
                onAfterRowEditorButtonClick: function(e) {
                    console.log('onAfterRowDuplicateButtonClick html');
                },
                onBeforeShowingEditorPopup: function(e) {
                    console.log('onBeforeShowingEditorPopup html');
                    //e.preventDefault();
                },
                onBeforeSettingsSaveButtonClick: function(e) {
                    console.log('onBeforeSaveButtonClick html');
                    //e.preventDefault();

                    //  if (_is_demo) {
                    //      $('#popup_demo').modal('show');
                    //      e.preventDefault();//return false
                    //  }
                },
                onPopupUploadImageButtonClick: function() {
                    console.log('onPopupUploadImageButtonClick html');
										$('#popup_demo').modal('show');
										return false;
                },
                onSettingsPreviewButtonClick: function(e, getHtml) {
                    console.log('onPreviewButtonClick html');
										$('#popup_demo').modal('show');
										return false;
                },

                onSettingsExportButtonClick: function(e, getHtml) {
                    console.log('onSettingsExportButtonClick html');
										$('#popup_demo').modal('show');
										return false;
                },
                onBeforeSettingsLoadTemplateButtonClick: function(e) {

                    $('.template-list').html('<div style="text-align:center">Loading...</div>');

                    $.ajax({
                        url: '<?php echo base_url(); ?>load_templates',
                        type: 'GET',
                        dataType: 'json',
                        success: function(data) {
                            if (data.code == 0) {
                                _templateItems = '';
                                _templateListItems = data.files;
                                for (var i = 0; i < data.files.length; i++) {
                                    _templateItems += '<div class="template-item" data-id="' + data.files[i].id + '">' +
                                        '<div class="template-item-delete" data-id="' + data.files[i].id + '">' +
                                        '<i class="fa fa-trash-o"></i>' +
                                        '</div>' +
                                        '<div class="template-item-icon">' +
                                        '<i class="fa fa-file-text-o"></i>' +
                                        '</div>' +
                                        '<div class="template-item-name">' +
                                        data.files[i].name +
                                        '</div>' +
                                        '</div>';
                                }
                                $('.template-list').html(_templateItems);
                            } else if (data.code == 1) {
                                $('.template-list').html('<div style="text-align:center">No items</div>');
                            }
                        },
                        error: function() {}
                    });
                },
                onSettingsSendMailButtonClick: function(e) {
                    console.log('onSettingsSendMailButtonClick html');
                    //e.preventDefault();
                },
                onPopupSendMailButtonClick: function(e, _html) {
                    console.log('onPopupSendMailButtonClick html');
                    _email = $('.recipient-email').val();
                    _element = $('.btn-send-email-template');

                    output = $('.popup_send_email_output');
                    var file_data = $('#send_attachments').prop('files');
                    var form_data = new FormData();
                    //form_data.append('attachments', file_data);
                    $.each(file_data, function(i, file) {
                        form_data.append('attachments[' + i + ']', file);
                    });
                    form_data.append('html', _html);
                    form_data.append('mail', _email);

                    $.ajax({
                        url: 'send.php', // point to server-side PHP script
                        dataType: 'json', // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,
                        type: 'post',
                        success: function(data) {
                            if (data.code == 0) {
                                output.css('color', 'green');
                            } else {
                                output.css('color', 'red');
                            }

                            _element.removeClass('has-loading');
                            _element.text('Send Email');

                            output.text(data.message);
                        }
                    });

                },
                onBeforeChangeImageClick: function(e) {
                    console.log('onBeforeChangeImageClick html');
                    loadImages();
                },
                onBeforePopupSelectTemplateButtonClick: function(dataId) {


                    $.ajax({
                        url: '<?php echo base_url();?>add_blank',
                        type: 'POST',
                        //dataType: 'json',
                        data: {
                            id: dataId
                        },
                        success: function(data) {
                            data = JSON.parse(data);
                            $('.content-wrapper .email-editor-elements-sortable').html('');
                            for (var i = 0; i < data.blocks.length; i++) {
                                _content = '';
                                _content += '<div class="sortable-row">' +
                                    '<div class="sortable-row-container">' +
                                    ' <div class="sortable-row-actions">';

                                _content += '<div class="row-move row-action">' +
                                    '<i class="fa fa-arrows-alt"></i>' +
                                    '</div>';


                                _content += '<div class="row-remove row-action">' +
                                    '<i class="fa fa-remove"></i>' +
                                    '</div>';


                                _content += '<div class="row-duplicate row-action">' +
                                    '<i class="fa fa-files-o"></i>' +
                                    '</div>';


                                _content += '<div class="row-code row-action">' +
                                    '<i class="fa fa-code"></i>' +
                                    '</div>';

                                _content += '</div>' +

                                    '<div class="sortable-row-content" data-id=' + data.blocks[i].block_id + ' data-types=' + data.blocks[i].property + '  data-last-type=' + data.blocks[i].property.split(',')[0] + '  >' +
                                    data.blocks[i].content +
                                    '</div></div></div>';
                                $('.content-wrapper .email-editor-elements-sortable').append(_content);

                            }


                        },
                        error: function(error) {
                            $('.input-error').text('Internal error');
                        }
                    });

                    //_emailBuilder.makeSortable();

                },
                onBeforePopupSelectImageButtonClick: function(e) {
                    console.log('onBeforePopupSelectImageButtonClick html');
                },
                onPopupSaveButtonClick: function() {
									
                  //$('#popup_demo').modal('show');
									//return false;
                },
                onUpdateButtonClick: function() {
									$('#popup_demo').modal('show');
									return false;
                }

            });
            _emailBuilder.setAfterLoad(function(e) {
							_emailBuilder.makeSortable();
							$('.elements-db').remove();

							setTimeout(function(){
								_emailBuilder.makeSortable();
								_emailBuilder.makeRowElements();
							},1000);
            });

        </script>
        <div style="display:none;" id="html_div">
        	<?php echo (!empty($magazinPageData[0]['page_content']))?$magazinPageData[0]['page_content']:''; ?>
		</div>
<script>
  function newPage(){
    $url = 'create-new-page';
    window.open(
      "<?php echo base_url() ?>"+$url,
      '_blank' // <- This is what makes it open in a new window.
    );
    // var x = 'yes';
    // $url = 'add-new-page?newPage='+x;
    // window.open(
    //   "<?php echo base_url() ?>"+$url,
    //   '_blank' // <- This is what makes it open in a new window.
    // );
}

var html = "";
  $(window).bind("load", function() {
      setTimeout(function(){ 
      	var data = $('#html_div').html();  
      	/*alert(data)*/
      	var id = "<?php echo (!empty($magazinPageData[0]['id']))?$magazinPageData[0]['id']:''; ?>";
      	if( id !=''){
      		$('#page_content').html('');
          $('#previewData').html('');
	      	setVal(data);
	      } 
      }, 1000);     
      
  });
  function setVal(html){
  	
      $('#page_content').html(html);
      $('#previewData').html(html);
  }
</script> 
<script type="text/javascript">
    function sendToSaveFunction(html){
      var newPage = "<?php echo $newPage; ?>";
      if (newPage == "no") 
      {
        var magazineId = "<?php echo $insertId; ?>";
        var pageNumber = "<?php echo $pageNumber; ?>";
        var url = "<?php echo base_url() ?>admin/AddNewPageController/store";
        $.ajax({
            type: 'POST',
            url: url,
            data:{
                'magazine_id': magazineId,
                'page_number': pageNumber,
                'page_content': html
            },
            //dataType: 'json',
            success: function (response) {
              var obj = JSON.parse(response);
              //alert(obj.magazine_id);
              window.location.href='<?php echo base_url() ?>add-page?magazineId='+obj.magazine_id+'';           
            },
            
        }); 
      }
      else
      {
        var url = "<?php echo base_url() ?>admin/MagazineController/store";
        $.ajax({
            type: 'POST',
            url: url,
            data:{
                'page_content': html
            },
            //dataType: 'json',
            success: function (response) {
              var obj = JSON.parse(response);
              var person = prompt("Please copy the link:", obj.pagelink);
              if (person == null || person == "") {
                txt = "User cancelled the prompt.";
              } else {
                window.close();
              }
            },
        }); 
      }
             
    }
</script>

<script>
$(document).on('change','#Fileinput',function(){
	var imgpreview=DisplayImagePreview(this);
	$(".img_preview").show();	
});	
$(document).on('click','#SaveImage',function(){
	var url="<?php echo base_url() ?>admin/AddNewPageController/uploadImage";
    ajaxFormSubmit(url,'#Ajaxform',function(output){
		var data=JSON.parse(output);
		if(data.status=='success'){
			
			$('.element-contenteditable').find("img").attr('src',"<?php echo base_url() ?>"+data.imagePath);
			$('.im_progress').fadeOut();
		}else{
			alert("Something went wrong.Please try again.");
			$(".img_preview").hide();
		}
    })	
});

function DisplayImagePreview(input){
	console.log(input.files);
	if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#img_preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
	
function getUrl()
{
	return "<?php echo base_url('add-page?magazineId='); ?><?php echo $insertId; ?>";
}
function getNextPageUrl()
{
  var pageNumber = '<?php echo $_GET['page']; ?>';
  var numberOfPage = '<?php echo $_GET['numberOfPages'] ?>';
  if (pageNumber != numberOfPage) {
    var value1 = parseInt(pageNumber);
    var nexNumber = value1 + 1;
    return "<?php echo base_url('add-new-page?page='); ?>"+nexNumber+"&magazineId=<?php echo $_GET['magazineId'] ?>&name=<?php echo $_GET['name'] ?>&numberOfPages=<?php echo $_GET['numberOfPages'] ?>&category_id=<?php echo $_GET['category_id'] ?>&expiry_date=<?php echo $_GET['expiry_date'] ?>";
  }else{
    return "javascript:void";
  }
}
function getPreviewPageUrl()
{
  var pageNumber = '<?php echo $_GET['page']; ?>';
  var numberOfPage = '<?php echo $_GET['numberOfPages'] ?>';
  if (pageNumber != 1) {
    var value1 = parseInt(pageNumber);
    var nexNumber = value1 - 1;
    return "<?php echo base_url('add-new-page?page='); ?>"+nexNumber+"&magazineId=<?php echo $_GET['magazineId'] ?>&name=<?php echo $_GET['name'] ?>&numberOfPages=<?php echo $_GET['numberOfPages'] ?>&category_id=<?php echo $_GET['category_id'] ?>&expiry_date=<?php echo $_GET['expiry_date'] ?>";
  }else{
    return "javascript:void";
  }
}
</script>
<script>
	$(document).on("change", "#videoUpd", function(evt) {
		if(this.files[0].type != 'video/mp4'){
			alert("Please select only MP4 videos. you have selected "+this.files[0].type);
			return false;
		}
		if(this.files[0].size <= 5120){
			alert("File size can't more than 1 MB");
			return false;
		}
		var url = "<?php echo base_url(); ?>admin/AddNewPageController/UploadVideo";
		ajaxFormSubmit(url,'#AjaxVideoform',function(output){
			var data=JSON.parse(output);
			if(data.status=='success'){
				var html = '<video width="100%" height="315" controls autoplay=""><source src="<?php echo base_url(); ?>'+data.videoPath+'" id="video_here" /></video>';
				$('#active').find('.youtube-frame').attr('id', '');
            	$('#active').find('.youtube-frame').attr('id', 'video');
			  	
			  	$('#active').find('#video').html(html);
			  	var $source = $('#video_here');
			  	//$source[0].src = URL.createObjectURL(this.files[0]);
			  	$source.parent()[0].load();
			}else{
				alert("Something went wrong.Please try again.");
				
			}				
		});
	});
	
	function ajaxFormSubmit(url, form, callback) {
    var formData = new FormData($(form)[0]);
    $.ajax({
        url: url,
        type: 'POST',
        data: formData,
        datatype: 'json',
        beforeSend: function() {
            // do some loading options
        },
        success: function(data) {
            callback(data);
        },
 
        complete: function() {
            // success alerts
        },
 
        error: function(xhr, status, error) {
            alert(xhr.responseText);
        },
        cache: false,
        contentType: false,
        processData: false
 
    });
 
}
</script>
