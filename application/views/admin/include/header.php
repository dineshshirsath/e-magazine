<?php if(!$this->session->userdata('user_id')){redirect(base_url('admin'));} ?>
<!DOCTYPE html>
<html>
	<head>
		  <title>Emagazine Dashboard</title>
		  <!-- Bootstrap 3.3.7 -->
		 <link href="//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		  <!-- Font Awesome -->
		  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome/css/font-awesome.min.css">
		  <!-- Ionicons -->
		  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/ionicons.min.css">
		  <!-- jvectormap -->
		  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/jquery-jvectormap.css">
		  <!-- Theme style -->
		  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/AdminLTE.min.css">
		  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
		  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/_all-skins.min.css">
		  <!-- data table -->
		  <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/dataTables.bootstrap.min.css">
		  <!-- bootstrap wysihtml5 - text editor -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap3-wysihtml5.min.css">	
		  <!-- Google Font -->
		  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
		  <script src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
  			<header class="main-header">

    		<!-- Logo -->
		    <a href="<?php echo base_url();?>admin-dashboard" class="logo">
		      	<!-- mini logo for sidebar mini 50x50 pixels -->
		      	<span class="logo-mini"><b>E</b>M</span>
		      		<!-- logo for regular state and mobile devices -->
		      	<span class="logo-lg"><b>E</b>Magazine</span>
		    </a>

    		<!-- Header Navbar: style can be found in header.less -->
    		<nav class="navbar navbar-static-top">
      		<!-- Sidebar toggle button-->
			      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			        	<span class="sr-only">Toggle navigation</span>
			      </a>
			      <!-- Navbar Right Menu -->
			      <div class="navbar-custom-menu">
			      	 	<ul class="nav navbar-nav">
			         	<!-- Messages: style can be found in dropdown.less-->
          					
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            
            <ul class="dropdown-menu">
             
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <?php
  $imageData = $this->CommonFunctionModel->checkProfileImage($this->session->userdata('user_id')); 
?>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
           <?php if (!empty($imageData[0]['image'])){ ?>
                <img src="<?php echo base_url() ?>assets/image/profile_Image/<?php echo $imageData[0]['image']; ?>" class="user-image" alt="User Image">
               <?php }else{ ?>                  
                <img src="<?php echo base_url() ?>assets/image/profile_Image/profile-picture.jpg" class="user-image" alt="User Image">
              <?php } ?>
              <span class="hidden-xs"><?php echo $this->session->userdata('user_name'); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <?php if (!empty($imageData[0]['image'])){ ?>
                  <img src="<?php echo base_url() ?>assets/image/profile_Image/<?php echo $imageData[0]['image']; ?>" class="img-circle" alt="User Image">
                <?php }else{ ?>                   
                  <img src="<?php echo base_url() ?>assets/image/profile_Image/profile-picture.jpg" class="img-circle" alt="User Image">
                <?php } ?>
                <p>
                  <?php echo $this->session->userdata('user_name'); ?>
                  <small></small>
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url(); ?>user-profile" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url() ?>admin-logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
 