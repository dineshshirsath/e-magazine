<!-- /.content-wrapper -->
		  	<footer class="main-footer">
			    <div class="pull-right hidden-xs">
			      	
			    </div>
		    	<strong>Copyright &copy; <?php echo date("Y"); ?> All Rights Reserved by <a href="http://orbs-solutions.com/" target="_blank">Orbs Solutions</a>.
		  	</footer>
			<!-- Control Sidebar -->
		  	<aside class="control-sidebar control-sidebar-dark">
		    	<!-- Create the tabs -->
		    	<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
		      		<li><a href="#" data-toggle="tab"><i class="fa fa-home"></i></a></li>
		      		<li><a href="#" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
		    	</ul>
		    	<!-- Tab panes -->
		    	<div class="tab-content">
		      		<!-- Home tab content -->
			      	<div class="tab-pane" id="control-sidebar-home-tab"></div>
			      	<!-- /.tab-pane -->
		    	</div>
		  	</aside>
			<!-- /.control-sidebar -->
			<!-- Add the sidebar's background. This div must be placed
			immediately after the control sidebar -->
			<div class="control-sidebar-bg"></div>
		</div>
		<!-- jQuery 3 -->
		
		<!-- Bootstrap 3.3.7 -->
		<script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/fastclick.js"></script>
		<script src="<?php echo base_url() ?>assets/js/adminlte.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/jquery.sparkline.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/jquery.slimscroll.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/dashboard2.js"></script>
		<script src="<?php echo base_url() ?>assets/js/demo.js"></script>
		<script src="<?php echo base_url() ?>assets/js/dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url() ?>assets/js/jquery.dataTables.min.js"></script>
		
		<script src="<?php echo base_url() ?>assets/js/icheck.min.js"></script>
<!-- CK Editor -->
<script src="<?php echo base_url() ?>assets/js/ckeditor/ckeditor.js"></script>
		


	</body>
</html>
<script type="text/javascript">
	
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass   : 'iradio_flat-blue'
    })

$(function(){	
    var start = new Date();
	// set end date to max one year period:
	var end = new Date(new Date().setYear(start.getFullYear()+1));

	$('#from_date').datepicker({
	    startDate : start,
	    endDate   : end
		// update "to_date" defaults whenever "from_date" changes
	}).on('changeDate', function(){
	    // set the "to_date" start to not be later than "from_date" ends:
	    $('#to_date').datepicker('setStartDate', new Date($(this).val()));
	}); 

	$('#to_date').datepicker({
	    startDate : start,
	    endDate   : end
		// update "from_date" defaults whenever "to_date" changes
	}).on('changeDate', function(){
	    // set the "from_date" end to not be later than "to_date" starts:
	    $('#from_date').datepicker('setEndDate', new Date($(this).val()));
	});
 });

$(function () {
    CKEDITOR.replace('editor01')
    $('.textarea').wysihtml5()
 })
$(function () {
    CKEDITOR.replace('editor02')
    $('.textarea').wysihtml5()
 })
$(function () {
    CKEDITOR.replace('editor03')
    $('.textarea').wysihtml5()
 })
$(function () {
    CKEDITOR.replace('editor04')
    $('.textarea').wysihtml5()
 })
</script>