
<aside class="main-sidebar" style="height: auto;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      	<!-- Sidebar user panel -->
      	<div class="user-panel">
      	 <?php
  $imageData = $this->CommonFunctionModel->checkProfileImage($this->session->userdata('user_id')); 
?>
        	<div class="pull-left image">
          		<?php if (!empty($imageData[0]['image'])){ ?>
                <img src="<?php echo base_url() ?>assets/image/profile_Image/<?php echo $imageData[0]['image']; ?>" class="img-circle" alt="User Image">
              <?php }else{ ?>                  
                <img src="<?php echo base_url() ?>assets/image/profile_Image/profile-picture.jpg" class="img-circle" alt="User Image">
              <?php } ?>
              
        	</div>
        	<div class="pull-left info">
          		<p><?php echo $this->session->userdata('user_name'); ?></p>
          		<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        	</div>
      	</div>
      	<!-- search form -->
      	<form action="#" method="get" class="sidebar-form">
        	<div class="input-group">
          		<input type="text" name="q" class="form-control" placeholder="Search...">
          		<span class="input-group-btn">
                	<button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  		<i class="fa fa-search"></i>
                	</button>
              	</span>
        	</div>
      	</form>
      	<!-- /.search form -->
      	<!-- sidebar menu: : style can be found in sidebar.less -->
      	<ul class="sidebar-menu" data-widget="tree">
        	<li class="header">MAIN NAVIGATION</li>
        	<li class="active treeview menu-open">
          		<a href="#">
            		<i class="fa fa-dashboard"></i> <span>Dashboard</span>
		            <span class="pull-right-container">
		              	<i class="fa fa-angle-left pull-right"></i>
		            </span>
          		</a>
        	</li>
        	<li class="treeview">
          		<a href="#">
	            	<i class="fa fa-edit"></i> <span>Category</span>
	            	<span class="pull-right-container">
	              		<i class="fa fa-angle-left pull-right"></i>
	            	</span>
          		</a>
          		<ul class="treeview-menu">
            		<li><a href="<?php echo base_url(); ?>category-listing"><i class="fa fa-circle-o"></i>Magazine Category</a></li>
          		</ul>
        	</li>
          <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i> <span>Magazine</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php echo base_url(); ?>add-page"><i class="fa fa-circle-o"></i>Create Magazine Page</a></li>
                <li><a href="<?php echo base_url(); ?>magazine-listing"><i class="fa fa-circle-o"></i>Magazine Listing</a></li>
              </ul>
          </li>
      	</ul>
    </section>
    <!-- /.sidebar -->
 </aside>