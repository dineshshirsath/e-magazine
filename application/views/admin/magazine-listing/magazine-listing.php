<style>
    .action-btns {
        display: inherit;
    }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
                Admin Dashboard    
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Magazine Listing</li>
          </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Info Content -->
        <?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
           <?php echo $this->session->flashdata('success'); ?>
        </div>

        <?php } else if($this->session->flashdata('error')){  ?>
        <div class="alert alert-danger">
          <a href="#" class="close" data-dismiss="alert">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
         <div class="box">
            <div class="box-header">
                  <h3 class="box-title">Magazine Listing</h3>
            </div>
            <!-- /.box-header -->
            <style>
            	.btn-group-lg>.btn, .btn-lg {
						    padding: 0px 3px;
						    font-size: 16px;
						    line-height: 1.3333333;
						    border-radius: 6px;
						}
            </style>
            <div class="box-body">
                <table class="table table-bordered table-striped dataTable" id="category">
                    <thead>
                        <th>Sr. No.</th>
                        <th>Magazine Name</th>
                        <th>Magazine Category</th>
                        <th>Magazine Number of Pages</th>
                        <th>Link</th>
                        <th>Action</th>
                    </thead>
                    <?php $inc = 1;
                        foreach($magazineList as $row)
                        { 
                       		$CreatedPageCnt = $this->MagazineModel->getMagazinePageCount($row['id']);
                        ?>
	                        <tr>
	                            <td><?php echo $inc++;?></td>
	                            <td><?php echo $row['magazine_name'];?></td>
	                            <td><?php echo $row['category_name'];?></td>
	                            <td><?php echo $row['number_of_page'];?></td>
	                            <?php
	                            $date = date("Y-m-d");
								$expiryDate = $row['expiry_date'];
									 
								?>
								<?php if($CreatedPageCnt >= $row['number_of_page'] ){ if ($date <= $expiryDate): ?>
	                            <td id="myInput_<?php echo $inc; ?>"><a href="<?php echo base_url().'magazine?id='.base64_encode($row['id']);?>" target="_blank"><?php echo base_url().'magazine?id='.base64_encode($row['id']);?></a></td>
	                            <?php else:	  ?>
	                            <td>Link is Expired</td>
	                            <?php endif; }else{ ?>
	                            <td>Please fill all pages. </td>
	                            <?php } ?>
	                            <td>
	                                <?php if($CreatedPageCnt >= $row['number_of_page'] ){ if ($date <= $expiryDate): ?>
                                       <div class="action-btns">
                                         <button title="Copy Link" class="btn-lg edit-btn" onClick="CopyToClipboard('myInput_<?php echo $inc; ?>')"><i class="fa fa-copy" aria-hidden="true"></i></button>
                                       </div>
                                    <?php endif; }?>
                                    <div class="action-btns">
                                         <a href="<?php echo base_url();?>add-page?magazineId=<?php echo $row['id']?>" style="color: black;"><button title="Edit" class="btn-lg edit-btn"><i class="fa fa-edit" aria-hidden="true"></i></button></a>
                                    </div>
                                    <div class="action-btns">
                                         <a href="<?php echo base_url();?>duplicate-magazine?magazineId=<?php echo $row['id']?>" style="color: black;"><button title="Duplicate Magazine" class="btn-lg edit-btn"><i class="fa fa-clipboard" aria-hidden="true"></i></button></a>
                                    </div>
                                    <div class="action-btns">
                                         <a href="javascript:void()" style="color: black;" onClick="doconfirm(<?php echo $row['id']?>);"><button title="Delete" class="btn-lg edit-btn"><i class="fa fa-trash-o" aria-hidden="true"></i></button></a>
                                    </div>
                            </td>
	                        </tr>
                        <?php }
                        ?>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </section>
    <!-- /.content -->
</div>

<script>
function CopyToClipboard (containerid) {
  // Create a new textarea element and give it id='temp_element'
  var textarea = document.createElement('textarea')
  textarea.id = 'temp_element'
  // Optional step to make less noise on the page, if any!
  textarea.style.height = 0
  // Now append it to your page somewhere, I chose <body>
  document.body.appendChild(textarea)
  // Give our textarea a value of whatever inside the div of id=containerid
  textarea.value = document.getElementById(containerid).innerText
  // Now copy whatever inside the textarea to clipboard
  var selector = document.querySelector('#temp_element')
  selector.select()
  document.execCommand('copy')
  // Remove the textarea
  document.body.removeChild(textarea)
	alert("Copied the text: " + textarea.value);
}
</script>


<script>
      $(function(){
          $('#category').dataTable({
            "processing": true,
            "bDestroy": true,
            "aaSorting": [[ 0,"asc" ]],
            "columnDefs":[
                {
                    "targets":[0,5],
                    "orderable":false,
                },
            ],
          });
      });

function doconfirm(magazineId)
{
    $url = 'delete-magazine-content/'+magazineId;

    if (confirm("Do you want to delete the magazine..?") == true)
    {
      window.location.href = "<?php echo base_url() ?>"+$url;
    }
    else
    {
      return false;
    }
}      
</script>
