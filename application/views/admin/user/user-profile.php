<div class="content-wrapper" style="min-height: 1126.3px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	    <h1>
	    		Admin Dashboard
	    </h1>
    	<ol class="breadcrumb">
	        <li><a href="<?php base_url(); ?>admin-dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
	        <?php if ($this->session->userdata('user_id')): ?>
	        	<li class="active">Admin Profile</li>
	    	<?php endif ?>
      	</ol>
    </section>
    <!-- Main content -->
    <section class="content">
		<?php if($this->session->flashdata('success')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
           <?php echo $this->session->flashdata('success'); ?>
        </div>

        <?php } else if($this->session->flashdata('error')){  ?>
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <?php echo $this->session->flashdata('error'); ?>
        </div>
        <?php } ?>
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
          <?php
  $imageData = $this->CommonFunctionModel->checkProfileImage($this->session->userdata('user_id')); 
?>
            <div class="box-body box-profile">
              <?php if ($imageData[0]['image'] != '' and $imageData[0]['image'] != 'null'){ ?>
                <a href="" data-toggle="modal" data-target="#modal-default"><img class="profile-user-img img-responsive img-circle" title="Upload Image" src="<?php echo base_url(); ?>assets/image/profile_Image/<?php echo $userData[0]['image'] ?>" alt="User profile picture" style="height: 100px;">
                </a>
              <?php }else{ ?>                   
                <a href="" data-toggle="modal" data-target="#modal-default"><img class="profile-user-img img-responsive img-circle" title="Upload Image" src="<?php echo base_url(); ?>assets/image/profile_Image/profilepicture.jpg" alt="User profile picture" style="height: 100px;">
                </a>
              <?php } ?>

              <h3 class="profile-username text-center"><?php echo $userData[0]['first_name']." ". $userData[0]['last_name']; ?></h3>

              <p class="text-muted text-center">
              	<?php if ($this->session->userdata('user_id')): ?>
		        	Admin
		    	<?php endif ?>
		      </p>

              <ul class="list-group list-group-unbordered">
                <hr>
              </ul>

              <a href="<?php echo base_url(); ?>admin-logout" class="btn btn-primary btn-block"><b>Sign out</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#activity" data-toggle="tab">Details</a></li>
              <li><a href="#timeline" data-toggle="tab">Update Profile</a></li>
              <li><a href="#settings" data-toggle="tab">Change Password</a></li>
            </ul>
            <div class="tab-content">
              <div class="active tab-pane" id="activity">
                <!-- Post -->
                <div class="box-body">
		            <div class="col-md-12">
		            	<table style="width: 100%; border: 1px solid #d4d4d4;">
		            		<tr style="height: 40px;">
		            			<td style="width:35%; padding-left: 10px;">Full Name</td>
		            			<td style="width:5%;">:</td>
		            			<td style="padding-left: 10px;"><?php echo $userData[0]['first_name']." ". $userData[0]['last_name']; ?></td>
		            		</tr>
		            		<tr style="height: 40px;">
		            			<td style="width:35%; padding-left: 10px;">Mobile</td>
		            			<td style="width:5%;">:</td>
		            			<td style="padding-left: 10px;"><?php echo $userData[0]['mobile']; ?></td>
		            		</tr>
		            		<tr style="height: 40px;">
		            			<td style="width:35%; padding-left: 10px;">Gender</td>
		            			<td style="width:5%;">:</td>
		            			<td style="padding-left: 10px;"><?php echo $userData[0]['gender']; ?></td>
		            		</tr>
		            		<tr style="height: 40px;">
		            			<td style="width:35%; padding-left: 10px;">Address</td>
		            			<td style="width:5%;">:</td>
		            			<td style="padding-left: 10px;"><?php echo $userData[0]['address']; ?></td>
		            		</tr>
		            		<tr style="height: 40px;">
		            			<td style="width:35%; padding-left: 10px;">E-mail Id</td>
		            			<td style="width:5%;">:</td>
		            			<td style="padding-left: 10px;"><?php echo $userData[0]['email']; ?></td>
		            		</tr></table>
		            </div>
		        </div>
                <!-- /.post -->
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="timeline">
                <!-- The timeline -->
                <form class="form-horizontal" action="<?php echo base_url(); ?>user-profile-update" method="post" enctype="multipart/form-data" role="form">
                  <div class="form-group">
                    <div class="col-sm-10">
                      <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $userData[0]['id']; ?>" placeholder="First Name">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">First Name</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="first_name" value="<?php echo $userData[0]['first_name']; ?>" placeholder="First Name">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Last Name</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="last_name" value="<?php echo $userData[0]['last_name']; ?>" placeholder="Last Name">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="email" id="inputEmail" value="<?php echo $userData[0]['email']; ?>" placeholder="Email">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Mobile</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="mobile" value="<?php echo $userData[0]['mobile']; ?>" placeholder="Mobile">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Gender</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="gender" id="inputSkills" value="<?php echo $userData[0]['gender']; ?>" placeholder="Gender">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputExperience" class="col-sm-2 control-label">Address</label>

                    <div class="col-sm-10">
                      <textarea class="form-control" name="address" id="inputExperience" placeholder="Address"><?php echo $userData[0]['address']; ?></textarea>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-primary ">Update</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="settings">
                <form class="form-horizontal" action="<?php echo base_url(); ?>user-change-password" method="post" enctype="multipart/form-data" role="form">
                  <div class="form-group">
                    <div class="col-sm-10">
                      <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $userData[0]['id']; ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputName" class="col-sm-4 control-label">New Password</label>

                    <div class="col-sm-4">
                      <input type="password" class="form-control" name="new_pwd" placeholder="New Password">
					  <span style="color: red;"><?php echo form_error('new_pwd');?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-4 control-label">Confirm Password</label>

                    <div class="col-sm-4">
                      <input type="password" class="form-control" name="confirm_pwd" placeholder="Conform Password">
					  <span style="color: red;"><?php echo form_error('confirm_pwd');?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-8">
                      <button type="submit" class="btn btn-primary" style="margin-left: 27%;">Change</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
</div>
    <div class="modal fade" id="modal-default">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Update Profile</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" action="<?php echo base_url(); ?>profile-image" method="post" enctype="multipart/form-data" role="form">
              <div class="form-group">
                <div class="col-sm-12">
                  <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $userData[0]['id']; ?>">
                </div>
              </div>
              <div class="col-sm-12">
                <input type="file" class="form-control" name="image" placeholder="Update Profile Picture">
                <span style="color: red;"><?php echo form_error('image');?></span>
              </div>            
              <br>
              <br>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Upload</button>
            </div>
          </form>  
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>