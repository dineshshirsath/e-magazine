<style>
	td.element-content {
    text-align: center;
}
</style>
<section class="content">
    <!-- Info Content -->
    <div align="center">
  		<?php $i=1; ?>
  		<?php foreach($pagedata as $row): ?>
    		<div style="width:650px;  border: 4px solid black;margin-bottom: 20px;padding-bottom: 30px;/*background-color:#e9e8e7;*/"> 
     			<?php echo ($row['page_content']); ?>
     			<hr style="width: 60%;border: 1px solid black;">
     			<div style="border-radius: 50px;width:20px;height: 20px;border:1px solid black;">
     				<?php echo $i++;?>
     			</div>
    		</div>
		<?php endforeach ?>
    </div>
</section>
<script>
  document.querySelectorAll("div[contenteditable]").forEach(function(el){
    el.removeAttribute("contenteditable");
  })
  document.querySelectorAll("td[contenteditable]").forEach(function(el){
    el.removeAttribute("contenteditable");
  })
  document.querySelectorAll("h1[contenteditable]").forEach(function(el){
    el.removeAttribute("contenteditable");
  })
</script>