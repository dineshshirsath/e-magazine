<style>
	td.element-content {
    text-align: center;
}
</style>
<?php
	$date = date("Y-m-d");
	$expiryDate = $pagedata[0]['expiry_date'];
 ?>
<?php if ($date <= $expiryDate): ?>
    <section class="content">
      <!-- Info Content -->
      <div align="center">
  		<?php $i=1; ?>
  		<?php if(!empty($pagedata[0]['magazine_name'])):?>
  		   <h2 style="text-decoration: underline;"><?php echo ($pagedata[0]['magazine_name']); ?></h2>
  		<?php endif ?>
  		<?php foreach($pagedata as $row): ?>
    		<div style="width:650px;  border: 4px solid black;margin-bottom: 20px;padding-bottom: 30px;/*background-color:#e9e8e7;*/"> 
     			<?php echo ($row['page_content']); ?>
     			<hr style="width: 60%;border: 1px solid black;">
     			<div style="border-radius: 50px;width:20px;height: 20px;border:1px solid black;">
     				<?php echo $i++;?>
     			</div>
    		</div>
		<?php endforeach ?>
	 </div>
    </section>
    <?php else:	  ?>
    <div style="background-color: lightgrey;width: 336px;border: 25px solid #375579; padding: 53px;margin: 226px;margin-left: 507px;">
       <h3 style="margin-left: 57px;">Magazine Link is Expired.</h3>
       <!--<a href="#" style="background-color: #4CAF50;border: none;color: white;padding: 15px 32px;text-align: center; text-decoration: none; display: inline-block;font-size: 16px;margin: 8px 89px;cursor: pointer;">
       Back to Home</a>-->
    </div>
	<?php endif ?>
    <!-- /.content -->

<script>
	$(window).bind("load", function() {
	   $('.mce-item-table').css('width', '0');
	   $('.sortable-row-actions').hide();
	});
	
</script>
<script>
  document.querySelectorAll("div[contenteditable]").forEach(function(el){
    el.removeAttribute("contenteditable");
  })
  document.querySelectorAll("td[contenteditable]").forEach(function(el){
    el.removeAttribute("contenteditable");
  })
  document.querySelectorAll("h1[contenteditable]").forEach(function(el){
    el.removeAttribute("contenteditable");
  })
  document.querySelectorAll("a[contenteditable]").forEach(function(el){
    el.removeAttribute("contenteditable");
  })
</script>