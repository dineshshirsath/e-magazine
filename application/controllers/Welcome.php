<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Welcome extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('MagazineModel');
		$this->load->model('HomeModel');
		$this->load->model('CategoryModel');
	}
	public function index()
	{ 
	  	if(!empty($_GET['id'])){
		    $id = base64_decode($_GET['id']);
			if($id){
				$data['pagedata']=$this->MagazineModel->pagedata($id);	
				$this->load->view('Home',$data);
			}
	  	}
	  	else{
	  		$data['searchListData'] = $this->HomeModel->getSearchListData();
	  		$data['categoryData'] = $this->CategoryModel->getCategoryData();
			$this->load->view('website/include/header');
			$this->load->view('website/pages/search-magazine', $data);
			$this->load->view('website/include/footer');
		}
	}
	public function Home()
	{
		$id = 1; 	
		$data['pagedata']=$this->MagazineModel->pagedata($id);		
		$this->load->view('welcome_message',$data);
	}
	public function getSearchData()
	{
		$searchData = $this->input->post('searchData');
	  	$data = $this->HomeModel->getSearchData($searchData);
	  	$i = 1;$html="";
	  	if (!empty($data)) 
	  	{
	  		foreach ($data as $row) 
		  	{
		  		$CreatedPageCnt = $this->MagazineModel->getMagazinePageCount($row['id']); 
                $date = date("Y-m-d");
                $expiryDate = $row['expiry_date'];
		  		if ($CreatedPageCnt >= $row['number_of_page']) {
		  			if ($date <= $expiryDate) {
		  				$html .="<div class='post'><div class='wrap-ut pull-left'><div class='userinfo pull-left'><div class='avatar'><img src='assets/website/images/maga.jpg' alt='' style='height: 35px; width: 35px;'><div class='status green'>&nbsp;</div></div></div><div class='posttext pull-left'><h2><a href='".base_url().'magazine?id='.base64_encode($row['id'])."' target='_blank'>".$row['magazine_name']."</a></h2><p><b>Category :</b>".$row['category_name']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Expiry :</b>".$row['expiry_date'] ."</p></div><div class='clearfix'></div></div><div class='postinfo pull-left'><div class='comments'><div class='commentbg'><p>".$row['number_of_page']." Pages</p><div class='mark'></div></div></div></div><div class='clearfix'></div></div>";
		  			}
		  		}
		  	}
	  		echo $html;
	  	}else{
	  		$html ="<div class='post'><div class='wrap-ut pull-left'><div class='posttext pull-left'><h2 style='margin-left:100px;'>Record not faund...!</h2><p></p></div><div class='clearfix'></div></div><div class='postinfo pull-left'><div class='comments'><div class='commentbg'><div class='mark'></div></div></div></div><div class='clearfix'></div></div>";
	  		echo $html;
	  	}
	  	
	}
	public function getScrollLoadData()
	{
		//Get last ID
		$lastID = $this->input->post('id');
		//Limit on data display
		$showLimit = 2;
		$html="";
		$postID = 0;
		$i = 1;
	  	$queryAll = $this->HomeModel->queryAllData($lastID);
	  	$allNumRows = $queryAll[0]['num_rows'];
	  	$query = $this->HomeModel->queryData($lastID);
	  	foreach ($query as $row) 
	  	{
	  		$CreatedPageCnt = $this->MagazineModel->getMagazinePageCount($row['id']); 
            $date = date("Y-m-d");
            $expiryDate = $row['expiry_date'];
	  		if ($CreatedPageCnt >= $row['number_of_page']) {
	  			if ($date <= $expiryDate) {
			  		$postID = $row["id"];
			  		$html .="<div class='post'><div class='wrap-ut pull-left'><div class='userinfo pull-left'><div class='avatar'><img src='assets/website/images/maga.jpg' alt='' style='height: 35px; width: 35px;'><div class='status green'>&nbsp;</div></div></div><div class='posttext pull-left'><h2><a href='".base_url().'magazine?id='.base64_encode($row['id'])."' target='_blank'>".$row['magazine_name']."</a></h2><p><b>Category :</b>".$row['category_name']."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Expiry :</b>".$row['expiry_date'] ."</p></div><div class='clearfix'></div></div><div class='postinfo pull-left'><div class='comments'><div class='commentbg'><p>".$row['number_of_page']." Pages</p><div class='mark'></div></div></div></div><div class='clearfix'></div></div>";
			  	}
			}
	  	}
	  	if ($allNumRows > $showLimit) {
	  			$html .="<div class='load-more' lastID=".$postID." style='display: none;'></div>";
  		}else{
  			$html .="<div class='load-more' lastID='0' style='text-align: center;'>That's All!</div>";
  		}
	  	echo $html;
	}
	
}
?>