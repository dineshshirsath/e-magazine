<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class UserController extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel');
	}

	public function index()
	{
		$data['userListingData'] = $this->UserModel->getUserListingData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.'user/user-listing', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function userProfile()
	{
		$data['userData'] = $this->UserModel->getProfileData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.'user/user-profile', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function update()
	{
		$id = $this->input->post('id');
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email'),
			'address' => $this->input->post('address'),
			'mobile' => $this->input->post('mobile'),
			'gender' => $this->input->post('gender')
		); 

		if ($this->UserModel->update($id, $data)) 
		{
			$this->session->set_flashdata('success', 'Record update successfully.');
				redirect(base_url() . 'user-profile');
		}
		else
		{
			$this->session->set_flashdata('error', 'Record not update.');
				//echo 'Not added record ';
			redirect(base_url() . 'user-profile');
		}
	}

	public function changePassword()
	{
		$this->form_validation->set_rules('new_pwd', 'New Password', 'trim|required|min_length[8]');
		$this->form_validation->set_rules('confirm_pwd', 'Confirm Password', 'trim|required|min_length[8]|matches[new_pwd]');

		if ($this->form_validation->run() == FALSE) 
		{
			$data['userData'] = $this->UserModel->getProfileData();
			$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
			$this->load->view(ADMIN.USER.'user-profile', $data);	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
		}
		else
		{
			$id = $this->input->post('id');
			$data = array(
				'password' => $this->input->post('new_pwd')
			);
			
			if ($this->UserModel->changePassword($id, $data)) 
			{
				$this->session->set_flashdata('success', 'Change password successfully.');
					redirect(base_url() . 'user-profile');
			}
			else
			{
				$this->session->set_flashdata('error', 'Password not change.');
					//echo 'Not added record ';
				redirect(base_url() . 'user-profile');
			}
		}
	}

	public function changeProfileImage()
	{
		$id = $this->input->post('id');
		$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/image/profile_Image/";
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['overwrite'] = FALSE;
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		if($this->upload->do_upload('image'))
		{
			$uploadData = $this->upload->data();
			$image = $uploadData['file_name'];
		}
		else
		{
			print_r($this->upload->display_errors());exit;
			$image = '';
		}
		$data = array(
			'image' => $image
		);

		if ($this->UserModel->changeProfileImageUpdate($id, $data)) 
		{
			$this->session->set_flashdata('success', 'Profile upload successfully.');
			redirect(base_url() . 'user-profile');
		}
		else
		{
			$this->session->set_flashdata('error', 'Profile not upload.');
			//echo 'Not added record ';
			redirect(base_url() . 'user-profile');
		}
	}

}
?>