<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_model');
	}
	public function index()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		if($this->form_validation->run() == TRUE){
			if($this->Login_model->checkLogin())
			{
				redirect(base_url('admin-dashboard'));
			}
			else{
				$this->session->set_flashdata('error','Enter a valid login details.');
				redirect(base_url('admin'));
			}
		}
		if(!$this->session->all_userdata())
		{
			redirect(base_url('admin-dashboard'));
		}
		$this->load->view(ADMIN.'login');	
	}
	public function logout()
	{
		$user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'user_name' && $key != 'user_email' && $key != 'id') {
                $this->session->unset_userdata($key);
            }
        }
		$this->session->sess_destroy();
		redirect(base_url('admin'));
	}
}
?>