<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AddNewPageController extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('AddNewPageModel');
	}
	public function index()
	{
		if (!empty($_GET['magazineId'])) 
		{
			$Id = $_GET['magazineId'];
			$category_id = $_GET['category_id'];
			$expiry_date = $_GET['expiry_date'];
			$magazineName = $_GET['name'];
			$numberOfPages = $_GET['numberOfPages'];
			
			$data = array(
				'magazine_name' => $magazineName,
				'number_of_page' => $numberOfPages,
				'expiry_date' => $expiry_date,
				'category_id' => $category_id
			);

			if ($this->AddNewPageModel->update($Id,$data)) 
			{
				$data['insertId'] = $_GET['magazineId'];
				$data['pageNumber'] = $_GET['page'];
				$data['magazinPageData'] = $this->AddNewPageModel->getMagazinePageData($_GET['magazineId'], $_GET['page']);
			$this->load->view(ADMIN.'add-new-page/add-new-page', $data);	
			}
			
		}
		else
		{
			$category_id = $_GET['category_id'];
			$expiry_date = $_GET['expiry_date'];
			$magazineName = $_GET['name'];
			$numberOfPages = $_GET['numberOfPages'];
			$data[] = array(
				'magazine_name' => $magazineName,
				'number_of_page' => $numberOfPages,
				'expiry_date' => $expiry_date,
				'category_id' => $category_id
			);

			if ($this->AddNewPageModel->insert($data)) 
			{
				$data['insertId'] = $this->db->insert_id();
				$data['pageNumber'] = $_GET['page'];	
				$this->load->view(ADMIN.'add-new-page/add-new-page', $data);	
			}
		}
						
	}
	public function add_blank()
	{
		$this->load->view(ADMIN.'add-new-page/get_template_blocks');	
	}
	public function load_templates()
	{
		$this->load->view(ADMIN.'add-new-page/load_templates');	
	}
	public function update_block_info()
	{
		$this->load->view(ADMIN.'add-new-page/update_block_info');	
	}
	public function template_import()
	{
		$this->load->view(ADMIN.'add-new-page/template_import');	
	}
	public function add_page()
	{
		$data['magazineId'] = "";
		$data['magazineData'] = "";

		if (!empty($_GET['magazineId'])) 
		{
			$magazineId = $_GET['magazineId'];
			$data['magazineId'] = $_GET['magazineId'];
			$data['magazineData'] = $this->AddNewPageModel->getMagazineDataById($magazineId);
		}
		$data['categoryData'] = $this->AddNewPageModel->getCategoryData();	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');
		$this->load->view(ADMIN.'add-page/add-page', $data);
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');

	}
	public function store()
	{
			// $data[] = array(
			// 	'magazine_id' => $this->input->post('magazine_id'),
			// 	'page_number' => $this->input->post('page_number'),
			// 	'page_content' => $this->input->post('page_content')
			// );
			// if ($this->AddNewPageModel->insertMagazineContent($data)) 
			// {
			// 	echo json_encode($data);
			// }

		$magazineId = $this->input->post('magazine_id');
		$pageNumber = $this->input->post('page_number');

		//$data['result'] = $this->AddNewPageModel->getDataAlreadyExists($magazineId, $pageNumber);

		if ($data['result'] = $this->AddNewPageModel->getDataAlreadyExists($magazineId, $pageNumber))
		{
			$id = $data['result']->id;
			
			$data = array(
				'magazine_id' => $this->input->post('magazine_id'),
				'page_number' => $this->input->post('page_number'),
				'page_content' => $this->input->post('page_content')
			);
			if ($this->AddNewPageModel->updateMagazinContent($data, $id)) 
			{
				$data[] = $data;
				echo json_encode($data);
			}
		}
		else
		{
			$data = array(
				'magazine_id' => $this->input->post('magazine_id'),
				'page_number' => $this->input->post('page_number'),
				'page_content' => $this->input->post('page_content')
			);
			if ($this->AddNewPageModel->insertMagazineContent($data)) 
			{
				$data[] = $data;
				echo json_encode($data);
			}
		}
		    
	}
	public function magazinePageContent()
	{
		$magazineId = $this->input->post('magazine_id');
		$pageNumber = $this->input->post('page_number');

		if ($this->AddNewPageModel->getDataAlreadyExists($magazineId, $pageNumber)) {
			$data = array(
					'exists' => '1' 
			);
			echo json_encode($data);
		}else{
			$data = array(
					'exists' => '0' 
			);
			echo json_encode($data);
		}
		
	}
	public function uploadImage()
	{
		$dir= "assets/uploads/";
		if(isset($_FILES['uploadImage'])){
			$imge=$_FILES['uploadImage'];
			if($imge['size']<=10000000){
				$destination=$dir.$imge['name'];
				$isuploaded=move_uploaded_file($imge['tmp_name'],$destination);
				if($isuploaded){
					echo json_encode(array("status"=>"success","message"=>"File has been uploaded successfully","imagePath"=>$destination));
				}else{
					echo json_encode(array("status"=>"fail","message"=>"Some error to upload this file"));
				}
			}else{
				echo json_encode(array("status"=>"fail","message"=>"File size can't more than 1 MB"));
			}
		}else{
			echo json_encode(array("status"=>"fail","message"=>"File size can't more than 1 MB"));
		}	
	}

	public function deleteMagazine()
	{
		$pageNumber = $_GET['page'];
		$magazineId = $_GET['magazineId'];
		if ($data = $this->AddNewPageModel->deleteMagazineContent($pageNumber, $magazineId))
		{
			$page_number = $data[0]['number_of_page'] - 1;
			$updateData = array(
				'number_of_page' => $page_number
			);
			if ($this->AddNewPageModel->updateNOP($magazineId, $updateData)) 
			{
				$i = 1;
				foreach ($data as $row) 
				{
					$numberData = array(
						'page_number' => $i
					);
					$this->AddNewPageModel->updatePageNumber($magazineId, $row['id'], $numberData);
					$i++;
				}				
			}
			echo "<script>alert('Record deleted successfully.'); window.location.href='".base_url().'add-page?magazineId='.$magazineId."'</script>";
			//return redirect(base_url().'add-page?magazineId='.$magazineId);
		}
	}

	public function getAllMagazineData()
	{
		$magazineId = $this->input->post('magazine_id');
		$divNumber = $this->input->post('div_Number');
		$text = explode(",", $divNumber);
		$i = 1;
		foreach ($text as $row) {
			$data = array(
				'order' => $i
			);
			if ($this->AddNewPageModel->setPageOrder($magazineId, $row, $data)){
				$update = 1;
			}else{
				$update = 0;
			}
			$i++;
		}
		if ($update == '1') {
			echo json_encode(array("status"=>"success","message"=>"Page set order by successfully"));
		}else{
			echo json_encode(array("status"=>"fail","message"=>"Page order by not set"));	
		}
	}

}
?>