<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class WebPageBuilderController extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('WebPageBuilderModel');
	}
	public function index()
	{
		$this->load->view(ADMIN.'page-builder/index');	
	}

}
?>