<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MagazineController extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('MagazineModel');
	}
	public function index()
	{
		$data['magazineList']=$this->MagazineModel->magazineList();	
		
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.'magazine-listing/magazine-listing',$data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function duplicateMagazine()
	{
		$magazineId = $_GET['magazineId'];
		$magazineData = $this->MagazineModel->getMagazineDadaById($magazineId);
		
		$data = array(
			'category_id' => $magazineData[0]['category_id'],
			'expiry_date' => $magazineData[0]['expiry_date'],
			'magazine_name' => $magazineData[0]['magazine_name'],
			'number_of_page' => $magazineData[0]['number_of_page']
		);
		if ($this->MagazineModel->insertDuplicateMagazine($data)) 
		{
			$insert_id = $this->db->insert_id();
			$magazinePageData = $this->MagazineModel->getMagazinePageContentById($magazineId);

			foreach ($magazinePageData as $row) 
			{
				$data = array(
					'magazine_id' => $insert_id,
					'page_number' => $row['page_number'],
					'page_content' => $row['page_content']
				);
				if($this->MagazineModel->insertDuplicateMagazinePageContent($data))
				{
					$result = 1;
				}else{
					$result = 0;
				}
			}
			if ($result = '1'){
				$this->session->set_flashdata('success', 'Created duplicate magazine successfully. Plese change magazine name..!');
				return redirect(base_url().'add-page?magazineId='.$insert_id);
			}			
		}
		else
		{
			$this->session->set_flashdata('error', 'Duplicate magazine not create.');
			return redirect(base_url().'magazine-listing');
		}
	}

	public function delete($id)
	{
		if ($this->MagazineModel->delete($id)) {
			$this->session->set_flashdata('success', 'Record deleted successfully...!');
			return redirect(base_url().'magazine-listing');
		}else{
			$this->session->set_flashdata('error', 'Record not deleted.');
			return redirect(base_url().'magazine-listing');
		}
	}

	public function store()
	{
		$data = array(
			'page_content' => $this->input->post('page_content')
		);
		if ($this->MagazineModel->insertNewPageContent($data)) 
		{
			$insert_id = $this->db->insert_id();
			echo json_encode(array("pagelink" => base_url().'new-page?id='.base64_encode($insert_id)));
		}
		    
	}

	public function newPage()
	{
		if(!empty($_GET['id'])){
		    $id = base64_decode($_GET['id']);
			if($id){
				$data['pagedata']=$this->MagazineModel->newPageData($id);	
				$this->load->view('new-page',$data);
			}
	  	}
	}
}
?>