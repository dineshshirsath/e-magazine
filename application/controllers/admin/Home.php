<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.'dashboard');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');	
	}
}
?>