<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CategoryController extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('CategoryModel');
	}

	public function index()
	{	
	    $data['categoryListingData'] = $this->CategoryModel->getCategoryListingData();
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.'category/category-listing', $data);	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
		
	}

	public function add()
	{	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.'category/add-category');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}
	
	public function insert()
	{
		$this->form_validation->set_rules('category_name', 'Category name', 'required');
		$this->form_validation->set_rules('category_description', 'Description', 'required');
		if ($this->form_validation->run() == FALSE) 
		{
			$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
			$this->load->view(ADMIN.'category/add-category');	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
		}
		else
		{
			$data=$this->input->post();
			unset($data['submit']);
			
			if ($this->CategoryModel->insert($data)) 
			{
				$this->session->set_flashdata('success', 'Record added successfully.');
				redirect(base_url() . 'category-listing');
			}
			else
			{
				$this->session->set_flashdata('error', 'Record not added.');
					//echo 'Not added record ';
				redirect(base_url() . 'category-listing');
			}
		}
	}

	public function updateView($id)
	{
		$data['categoryUpdatData'] = $this->CategoryModel->getUpdatCategoryData($id);
		$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
		$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
		$this->load->view(ADMIN.'category/edit-category', $data);		
		$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
	}

	public function update()
	{
		$this->form_validation->set_rules('category_name', 'Category name', 'required');
		$this->form_validation->set_rules('category_description', 'Description', 'required');
		$id = $this->input->post('id');
		
		if ($this->form_validation->run() == FALSE) 
		{
			$data['categoryUpdatData'] = $this->CategoryModel->getUpdatCategoryData($id);
			$this->load->view(ADMIN.ADMIN_INCLUDE.'header');	
			$this->load->view(ADMIN.ADMIN_INCLUDE.'menu');	
			$this->load->view(ADMIN.'category/edit-category', $data);		
			$this->load->view(ADMIN.ADMIN_INCLUDE.'footer');
		}
		else
		{
			$data = $this->input->post();
			unset($data['submit']);

			if ($this->CategoryModel->update($data, $id)) 
			{
				$this->session->set_flashdata('success', 'Record updated successfully.');
				redirect(base_url() . 'category-listing');
			}
			else
			{
				$this->session->set_flashdata('error', 'Record not updated.');
					//echo 'Not added record ';
				redirect(base_url() . 'category-listing');
			}
		}
	}

	public function delete($id)
	{
		if ($this->CategoryModel->delete($id)) 
		{
			$this->session->set_flashdata('success', 'Record deleted successfully.');
			redirect(base_url() . 'category-listing');
		}
		else
		{
			$this->session->set_flashdata('error', 'Record not deleted.');
				//echo 'Not added record ';
			redirect(base_url() . 'category-listing');
		}
	}
}
?>