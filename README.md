# E-magazine

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	    <h1>
	        Dashboard
	    </h1>
    	<ol class="breadcrumb">
	        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	        <li class="active">Dashboard</li>
      	</ol>
    </section>
    <!-- Main content -->
    <!--<section class="content">-->
      <!-- Info Content -->
      <style>
      	.pageDiv{
		    margin: 5px;
		    float: left;
		    width: 140px;
			height: 175px;
		}
		.page {
		    border: 1px solid #ccc;
		    width: 100%;
			height:150px;
		}
		.page:hover {
		    border: 1px solid #777;
		}
		.desc {
			margin-top:65px;
		    text-align: center;
		}
		.det {
		    text-align: center;
		    color: red;
		}
	</style>
	<section class="content">
    	<div class="container-fluid">
			<div class="row">
				 <div class="row">
					<div class="col-md-12">
				        <!-- general form elements -->
				        <div class="box box-primary">
				            <div class="box-header with-border">
				              	<h3 class="box-title">Magazine Pages Creator</h3>
				            </div>
				            <!-- /.box-header -->
				            <!-- form start -->
				            <?php
				            	if (empty($magazineId)) {
				            	 	$magazineId = '0';
				            	 }
				            	 if (empty($magazineData[0]['id'])) {
				            	 	$pageContentId = '0';
				            	 }else{
				            	 	$pageContentId = $magazineData[0]['id'];
				            	 } 
				            ?>
				            <form action="#" method="post" enctype="multipart/form-data" role="form">
				              	<div class="box-body">
									<input type="hidden" class="form-control" name="magazineId" id="magazineId" value="<?php echo $magazineId; ?> "><br>
									<input type="hidden" class="form-control" name="pageContentId" id="pageContentId" value="<?php echo $pageContentId; ?>">
						            <?php $categoryId = (!empty($magazineData[0]['category_id']))?$magazineData[0]['category_id']:""; ?>
						            <div class="col-md-12">
						            	<div class="form-group">
											<label class="control-label">Category Name</label>
											<select name="category_id" id="category_id" class="form-control">
												<option value="">-----Select Category Name-----</option>
												<?php foreach ($categoryData as $row): ?>
												<option value="<?php echo $row['id'] ?>" <?php echo ($row['id']== $categoryId)?"selected":""; ?>> <?php echo $row['category_name'] ?></option>
												<?php endforeach ?>
											</select>
											<span style="color: red;"><?php echo form_error('category_id');?></span>
										</div>
						            </div>
						            <div class="col-md-12">
						              <div class="form-group">
                                        <label>Expiry Date:</label>
                                          <div class="input-group date">
							                  <div class="input-group-addon">
							                    <i class="fa fa-calendar"></i>
							                  </div>
                                            <input type="date" class="form-control pull-right" id="expiry_date" name="expiry_date" value="<?php if(!empty($magazineData[0]['expiry_date'])){ echo $magazineData[0]['expiry_date'];} ?>" placeholder="Enter Magazine Expiry Date" required/><br>
											<span id="expiry_date" style="color: red;"></span>
                                           </div>
						                <!-- /.input group -->
						              </div>
						            </div>
									 <div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">Magazine Name:</label>
											<input type="text" class="form-control" name="magazineName" id="magazineName" value="<?php if(!empty($magazineData[0]['magazine_name'])){ echo $magazineData[0]['magazine_name'];} ?>" placeholder="Enter Magazine Name" required/><br>
											<span id="NameError" style="color: red;"></span>
					                	</div>
						            </div>
						            
					                <div class="col-md-12">
					                	<div class="form-group">
					                  		<label class="control-label">Number of Magazine Page:</label>
											<input type="number" class="form-control" name="pages" id="pages" value="<?php if(!empty($magazineData[0]['number_of_page'])){ echo $magazineData[0]['number_of_page'];} ?>" placeholder="Enter Number of Magazine Pages" onkeyup="getPages(this.value)" maxlength="2"><br>
											<span id="PageError" style="color: red;"></span>
											<div id="content"></div>
					                	</div>
						            </div>
						        </div>	
				            </form>
				        </div>
				    <!-- /.box -->
				    </div>
			    </div>
			</div>
		</div>
	</section>
<!-- $('select[name="package_id"]').val(); -->
</div>	
<script>
$(document).ready(function(){
	var numberOfPages = document.getElementById("pages").value;
	if (numberOfPages != null) {
		var i = 1;
		var html ="";
		for(i = 1;i <= numberOfPages;i++)
		{
			var magazineId = document.getElementById("magazineId").value;
			var magazineName = document.getElementById("magazineName").value;
			var category_id = document.getElementById("category_id").value;
			var expiry_date = document.getElementById("expiry_date").value;
			var existsPageContent = document.getElementById("pageContentId").value;
			if (i == 1){
				if (magazineId == 0) {
					html +='<div class="pageDiv"><a href="javascript:void()" onclick="goForEdit('+magazineId+','+i+')"><div class="page"><div class="desc">Page '+i+' <br> <span style="color: red;">Please fill this first page.</span></div></div></a></div>';
				}else{
					if (existsPageContent == 0){
						html +='<div class="pageDiv"><a href="javascript:void()"  onclick="goForEdit('+magazineId+','+i+')" ><div class="page"><div class="desc">Page '+i+' <br> <span style="color: red;">Please fill this first page.</span></div></div></a></div>';
					}else{
						html +='<div class="pageDiv"><a href="javascript:void()"  onclick="goForEdit('+magazineId+','+i+')" ><div class="page"><div class="desc">Page '+i+' <br> <span style="color: red;">Edit</span></div></div></a><a href="javascript:void()" onclick="goForDelete('+magazineId+','+i+')"><div class="det">Delete</div></a></div>';
					}
				}
				document.getElementById("content").innerHTML = html;	
				document.getElementById("NameError").innerHTML = "";
			}else{
				 if (magazineId == 0) {
				 	alert('hiii');
				 	html +='<div class="pageDiv"><a href="#"><div class="page"><div class="desc">Page '+i+'</div></div></a></div>';
				 }else{
				 	var url = "<?php echo base_url(); ?>admin/AddNewPageController/magazinePageContent";
				 	$.ajax({
				 		type: 'POST',
			          	url: url,
			          	async: false,
			           	data:{
			               	'magazine_id': magazineId,
			               	'page_number': i
			          	},
			         	//dataType: 'json',
			        	success: function (response) {
             				var obj = JSON.parse(response);
             				var exists = obj.exists;
             				//alert(exists);
             				if (obj.exists == 1) {
             					html +='<div class="pageDiv"><a href="javascript:void()" onclick="goForEdit('+magazineId+','+i+')" ><div class="page"><div class="desc">Page '+i+'<br><span style="color: red;">Edit</span></div></div></a><a href="javascript:void()" onclick="goForDelete('+magazineId+','+i+')"><div class="det">Delete</div></a></div>';
             				}else{
             					html +='<div class="pageDiv"><a href="javascript:void()" onclick="goForEdit('+magazineId+','+i+')" ><div class="page"><div class="desc">Page '+i+'</div></div></a></div>';
             				}
             				document.getElementById("content").innerHTML = html;	
							document.getElementById("NameError").innerHTML = "";
			           	},			          
			       	});
				 }
			}
			
		}
		if(document.getElementById("magazineName").value !=''){
			//document.getElementById("content").innerHTML = html;	
			//document.getElementById("NameError").innerHTML = "";
		}else{
			//document.getElementById("NameError").innerHTML = "Please enter magazine name!";
		}
	}
})

function getPages(val)
{
	var html ="";
	var i = 1;var fill = 0;
	for(i = 1;i <= val;i++)
	{
		var magazineId = document.getElementById("magazineId").value;
		var magazineName = document.getElementById("magazineName").value;
		var category_id = document.getElementById("category_id").value;
		var expiry_date = document.getElementById("expiry_date").value;
		var numberOfPages = document.getElementById("pages").value;
		var existsPageContent = document.getElementById("pageContentId").value;
		if (i == 1){
			
			if (magazineId == 0) {
				html +='<div class="pageDiv"><a href="add-new-page?page='+i+'&name='+magazineName+'&numberOfPages='+numberOfPages+'&category_id='+category_id+'&expiry_date='+expiry_date+'" ><div class="page"><div class="desc">Page '+i+' <br> <span style="color: red;"</style>Please fill this first page.</span></div></div></a></div>';
			}else{
				if (existsPageContent == 0){
					html +='<div class="pageDiv"><a href="javascript:void()"  onclick="goForEdit('+magazineId+','+i+')" ><div class="page"><div class="desc">Page '+i+' <br> <span style="color: red;">Please fill this first page.</span></div></div></a></div>';
				}else{
					html +='<div class="pageDiv"><a href="javascript:void()"  onclick="goForEdit('+magazineId+','+i+')" ><div class="page"><div class="desc">Page '+i+' <br> <span style="color: red;">Edit</span></div></div></a><a href="javascript:void()" onclick="goForDelete('+magazineId+','+i+')"><div class="det">Delete</div></a></div>';
				}
			}
			document.getElementById("content").innerHTML = html;	
			document.getElementById("NameError").innerHTML = "";
		}else{
			if (magazineId == 0) {
			 	html +='<div class="pageDiv"><a href="#"><div class="page"><div class="desc">Page '+i+'</div></div></a></div>';
			 }else{
			 	var url = "<?php echo base_url(); ?>admin/AddNewPageController/magazinePageContent";
			 	$.ajax({
			 		type: 'POST',
		          	url: url,
		          	async: false,
		           	data:{
		               	'magazine_id': magazineId,
		               	'page_number': i
		          	},
		         	//dataType: 'json',
		        	success: function (response) {
         				var obj = JSON.parse(response);
         				var exists = obj.exists;
         				//alert(exists);
         				if (obj.exists == 1) {
         					html +='<div class="pageDiv"><a href="javascript:void()" onclick="goForEdit('+magazineId+','+i+')" ><div class="page"><div class="desc">Page '+i+'<br><span style="color: red;">Edit</span></div></div></a><a href="javascript:void()" onclick="goForDelete('+magazineId+','+i+')"><div class="det">Delete</div></a></div>';
         					fill = 1;
         				}else{
         					if(fill == 1){
								html +='<div class="pageDiv"><a href="javascript:void()" onclick="goForEdit('+magazineId+','+i+')" ><div class="page"><div class="desc">Page '+i+'<br><span style="color: red;">Fill this page.</span></div></div></a></div>';
								fill = 0;
							}else{
								html +='<div class="pageDiv"><a href="javascript:void()" onclick="goForEdit('+magazineId+','+i+')" ><div class="page"><div class="desc">Page '+i+'</div></div></a></div>';
							}
         					
         				}
         				document.getElementById("content").innerHTML = html;	
						document.getElementById("NameError").innerHTML = "";
		           	},			          
		       	});
			 }			
		}			
	}		
	if(document.getElementById("magazineName").value !=''){
		document.getElementById("content").innerHTML = html;	
		document.getElementById("NameError").innerHTML = "";
	}else{
		document.getElementById("NameError").innerHTML = "Please enter magazine name!";
	}
		
}
</script> 
<script>
	function goForDelete(magzine,pages){
		var page = pages;
		var magazineId = magzine;
		$url = 'delete-magazine?page='+page+'&magazineId='+magazineId;

        if (confirm("Do you want to delete the record?") == true)
        {
          window.location.href = "<?php echo base_url() ?>"+$url;
        }
        else
        {
          return false;
        }
    } 
</script>
<script>
	function goForEdit(magzine,page){
	
		//window.location.replace("http://stackoverflow.com");
		numberOfPages = document.getElementById("pages").value;
		var magazineId = document.getElementById("magazineId").value;
		var magazineName = document.getElementById("magazineName").value;
		var category_id = document.getElementById("category_id").value;
		var expiry_date = document.getElementById("expiry_date").value;
		$link = 'add-new-page?page='+page+'&magazineId='+magazineId+'&name='+magazineName+'&numberOfPages='+numberOfPages+'&category_id='+category_id+'&expiry_date='+expiry_date;
		window.location.href = "<?php echo base_url() ?>"+$link;
	}
</script>

<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<!-- bootstrap time picker -->
<script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script>	
